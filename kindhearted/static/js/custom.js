jQuery(document).ready(function ($) {
    $('.nav li, .nav li').on({
        mouseenter: function() {
            $(this).children('ul').stop(true, true).slideDown(400);
        },
        mouseleave: function() {
            $(this).children('ul').slideUp(100);
        }
    });
    
    
    /* BX slider 1*/
    var bannerslider = $('#banner_slider');
    if (bannerslider.length) {
        bannerslider.bxSlider({ auto: true, minSlides: 1, maxSlides: 1, slideMargin: 18, speed: 500 });
    }
    var newslider = $('#news_slider');
    if (newslider.length) {
        newslider.bxSlider({ minSlides: 1, maxSlides: 1, slideMargin: 18, speed: 500 });
    }
    var videoslider = $('.video_slider');
    if (videoslider.length) {
        videoslider.bxSlider({ minSlides: 1, maxSlides: 1, slideMargin: 25, speed: 500, });
    }
    var blogslider = $('#blog_slider');
    if (blogslider.length) {
        blogslider.bxSlider({ minSlides: 1, maxSlides: 1 });
    }
    var shopslider = $('#shop_slider');
    if (shopslider.length) {
        shopslider.bxSlider({ slideWidth: 140,minSlides: 1, maxSlides: 3, slideMargin: 28 });
    }
    var officeslider = $('#office_slider');
    if (officeslider.length) {
        officeslider.bxSlider({ slideWidth: 270, minSlides: 1, maxSlides: 4, slideMargin: 28 });
    }
    var productslider = $('#slider_products');
    if (productslider.length) {
       productslider.bxSlider({ slideWidth: 270, minSlides: 1, maxSlides: 1, slideMargin: 10 });
    }

    /* bootstrap Add class to accordion **/
    var sidebar = $('.accordion-heading'); /* cache sidebar to a variable for performance */
    sidebar.delegate('.accordion-toggle', 'click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).addClass('inactive');
            $("#icon_toggle i", this).removeClass('icon-minus').addClass('icon-plus');
        } else {
            sidebar.find('.active').addClass('inactive');
            sidebar.find('.active').removeClass('active');
            $(this).removeClass('inactive');
            $(this).addClass('active');
            $("#icon_toggle i", this).removeClass('icon-plus').addClass('icon-minus');
        }
    });
    /* End of bootstrap Add class to accordion **/

    /* Footer Gallery Pretty Photo Widget **/
    $(".gallery-list:first a[data-rel^='prettyPhoto']").prettyPhoto({ animation_speed: 'normal', theme: 'light_square', slideshow: 3000, autoplay_slideshow: true });
    /* End of Footer Gallery Pretty Photo Widget **/
    
    /* Start of Photo Gallery Pretty Photo **/
    $(".gallery-page:first a[data-rel^='prettyPhoto']").prettyPhoto({animation_speed: 'normal',theme: 'light_square', slideshow: 3000, autoplay_slideshow: true });
    /* End of Photo Gallery Pretty Photo **/
    
    /* Social Icons **/
    $('.social_active').hoverdir({});
    /* End of Social Icons Animation **/

    /* Start of Counter */
    var austDay = new Date();
    austDay = new Date(2017, 2 - 1, 21, 11, 00)
    $('#countdown162').countdown({
        until: austDay
    });
    $('#year').text(austDay.getFullYear());     
    /* End of Counter */
    
     /* Bootstrap Tooltip */
     // $("a[data-rel='tooltip']").tooltip();
     /* Bootstrap Tooltip */
});


// $('.counter').each(function() {
//   var $this = $(this),
//       countTo = $this.attr('data-count');
  
//   $({ countNum: $this.text()}).animate({
//     countNum: countTo
//   },

//   {

//     duration: 8000,
//     easing:'linear',
//     step: function() {
//       $this.text(Math.floor(this.countNum));
//     },
//     complete: function() {
//       $this.text(this.countNum);
//       //alert('finished');
//     }

//   });  
  
  

// });




/*resetting form data on modal hide */
$('.modal_close').click(function(){
    form_id = $(this).closest('form').attr("id");
    $('#'+form_id)[0].reset();
})
/* end reseting */

/* entity form - for Occupation */
$('#occupation').change(function(){
    console.log("occupation clicked");
    
    occupation = $(this).children(":selected").attr('value');
    console.log(occupation);
    if(occupation=="business")
    {   
        console.log("business");
        $(".business").removeClass("hidebusiness");
        $(".professional").addClass("hideprofessional");
        $(".service").addClass("hideservice");
        $(".occ_others").addClass("hideocc_others");

    } else if(occupation=="professional")
    {
        $(".business").addClass("hidebusiness");
        $(".professional").removeClass("hideprofessional");
        $(".service").addClass("hideservice");
        $(".occ_others").addClass("hideocc_others");

    } else if(occupation=="service")
    {
        $(".business").addClass("hidebusiness");
        $(".professional").addClass("hideprofessional");
        $(".service").removeClass("hideservice");
        $(".occ_others").addClass("hideocc_others");

    } else if(occupation=="others")
    {
        $(".business").addClass("hidebusiness");
        $(".professional").addClass("hideprofessional");
        $(".service").addClass("hideservice");
        $(".occ_others").removeClass("hideocc_others");
    }
})

/* tooltips for sign up forms */

$('#signup_password').tooltip({'trigger':'focus', 'title': 'Password Should Contain Atleast 1 Uppercase , 1 Lowercase and 1 Special Character'});
$('#signup_username').tooltip({'trigger':'focus', 'title': 'Username Can Contain alphabets and digits with no Spaces'});

$('#signup_confirm_password').tooltip({'trigger':'focus', 'title': 'This field Should match with Password Field'});
$('#signup_name').tooltip({'trigger':'focus', 'title': 'Name Can Contain alphabets and digits and Spaces'});
$('#signup_email').tooltip({'trigger':'focus', 'title': 'Username Can Contain alphabets and digits with no Spaces'});
$('#signup_mobile_number').tooltip({'trigger':'focus', 'title': 'Corporate Entity can login with Mobile Number as well'});
$('#signup_section12a').tooltip({'trigger':'focus', 'title': 'If not registered under income tax act U/S 12A or 10(23) then Registered as OTHERS type User'});
$('#signup_website').tooltip({'trigger':'focus', 'title': 'For Eg. www.abc.com'});
/* end tooltips for sign up forms */



// csr_value = ""
// $('#csr_applicability').change(function(){
 
//  var csr_single_value = $('#csr_applicability option:selected').attr('value') + csr_value ;
//  csr_value = csr_value + "," + csr_single_value  
//  alert(csr_value);
// });


/* End of Counter */
// $('#signup').parsley()
// $(function () {
//   $('#signup').parsley().on('field:validated', function() {
//     var ok = $('.parsley-error').length === 0;
//     console.log("ok")
//     console.log(ok)
//     $('.bs-callout-info').toggleClass('hidden', !ok);
//     $('.bs-callout-warning').toggleClass('hidden', ok);
//   })
//   .on('form:submit', function(e) {
//     e.preventDefault();
//     var form = $(this);
//     if(form.parsley().isValid())
//     {
//         return true; 
//         console.log("validation successful")    
//     }
//     return false;
//   });
// });


// $('.cause-click').click(function(){
//     alert("test");
//     id = $(this).attr('id');
//     alert(id);
// });
//parsley multi step form validation
$(function () {
  var $sections = $('.form-section');

  function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
      .removeClass('current')
      .eq(index)
        .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
    var atTheEnd = index >= $sections.length - 1;
    $('.form-navigation .next').toggle(!atTheEnd);
    $('.form-navigation [type=submit]').toggle(atTheEnd);
  }

  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.form-navigation .previous').click(function() {
    navigateTo(curIndex() - 1);
  });

  // Next button goes forward iff current block validates
  $('.form-navigation .next').click(function() {
    if ($('.entity-form').parsley().validate({group: 'block-' + curIndex()}))
    {
        navigateTo(curIndex() + 1);
        $('#startproject-stp1').attr('data-target','#projectdetails');
        $(".btn-pref .basics").removeClass("btn-primary").addClass("btn-default");
        $(".btn-pref .story").removeClass("btn-default").addClass("btn-primary");
    }
        
        
      
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
  });
  navigateTo(0); // Start at the beginning
});


// end multi step form

// Binding next button on first step

  $("#corporate-1-nxt").click(function() {
        // $("#mem-corporate .company1").hide("fast");
        $("#mem-corporate .company1").addClass("company-st-1")
        $("#mem-corporate .company2").removeClass("company-st-2")
        //$("#mem-corporate .company2").show("slow");
   });
 
   // Binding next button on second step

   $("#ngo-1-nxt").click(function() {
      
        // $("#mem-ngo .ngo1").hide("fast");
        $('#member_ngo').parsley().validate()
        $("#mem-ngo .ngo1").addClass("ngo-st-1");
        $("#mem-ngo .ngo2").removeClass("ngo-st-2");
        //$("#mem-ngo .ngo2").show("slow");
      
    });
   $("#ngo-2-nxt").click(function() {
      
        // $("#mem-ngo .ngo2").hide("fast");
        $("#mem-ngo .ngo2").addClass("ngo-st-2");
        $("#mem-ngo .ngo3").removeClass("ngo-st-3");
        //$("#mem-ngo .ngo3").show("slow");
      
    });
   $("#ngo-3-nxt").click(function() {
      
        //$("#mem-ngo .ngo3").hide("fast");
        $("#mem-ngo .ngo3").addClass("ngo-st-3");
        $("#mem-ngo .ngo4").removeClass("ngo-st-4");
        //$("#mem-ngo .ngo4").show("slow");
      
    });
   
 
     // Binding back button on second step
    $("#corporate-2-bck").click(function() {
      // $("#mem-corporate .company2").hide("fast");
      $("#mem-corporate .company2").addClass("company-st-2")
      $("#mem-corporate .company1").removeClass("company-st-1")
      //$("#mem-corporate .company1").show("slow");
    });
    $("#ngo-2-bck").click(function() {
      // $("#mem-ngo .ngo2").hide("fast");
      $("#mem-ngo .ngo2").addClass("ngo-st-2")
      $("#mem-ngo .ngo1").removeClass("ngo-st-1")
      //$("#mem-ngo .ngo1").show("slow");
    });
    $("#ngo-3-bck").click(function() {
      // $("#mem-ngo .ngo3").hide("fast");
      $("#mem-ngo .ngo3").addClass("ngo-st-3")
      $("#mem-ngo .ngo2").removeClass("ngo-st-2")
      //$("#mem-ngo .ngo2").show("slow");
    });
    $("#ngo-4-bck").click(function() {
      // $("#mem-ngo .ngo4").hide("fast");
      $("#mem-ngo .ngo4").addClass("ngo-st-4")
      $("#mem-ngo .ngo3").removeClass("ngo-st-3")
     // $("#mem-ngo .ngo3").show("slow");
    });

// $('#membercontent').hide();
// $('#member_action').hide();
// $('#addactivity').click(function(){
//     console.log("test activity");
//     $('#member_action').show();
// });

 
$('#intenddonate').hide();    
$('#intendreceive').hide();
$('#request_type').hide();
$('#intendproject').hide();

$('input:radio[name="memberintend"]').change(function(){
    selectedelement = $(this).attr('value');
    if(selectedelement=="receive")
    {   
        $('#membercontent').hide();
        $('#intenddonate').hide();
        $('#request_type').show();
        
    }
    else if(selectedelement=="donate")
    {   
        $('#membercontent').show();
        $('#intenddonate').show();
        $('#request_type').hide();
        
    }
});

$('input:radio[name="requesttype"]').change(function(){
    selectedelement = $(this).attr('value');
    if(selectedelement == "general")
    {
        $('#intendreceive').show();
        $('#intendproject').hide();
        $('#membercontent').show();
    } else if(selectedelement == "project")
    {
        $('#intendreceive').hide();
        $('#intendproject').show();
        $('#membercontent').show();
    }
})
//end test


$('input:radio[name="membertype"]').change(function(){
   
    selectedradioid=$(this).attr('id');
    if(selectedradioid=="ngoradio")
    {
        $(".company").addClass("hidecompany");
        $(".others").addClass("hideothers");
        $(".ngo").removeClass("hidengo");

    }
    else if(selectedradioid=="individualradio")
    {
        $(".ngo").addClass("hidengo");
        $(".others").addClass("hideothers");
        $(".company").removeClass("hidecompany");
    }
    else if(selectedradioid=="othersradio")
    {
        $(".ngo").addClass("hidengo");
        $(".company").addClass("hidecompany");
        $(".others").removeClass("hideothers");
    }    
});

//hide show donate and request forms.

$('input:radio[name="Intends"]').change(function(){
   
    selectedradioid=$(this).attr('id');
    if(selectedradioid=="intenddonate")
    {
        
        $(".receive").addClass("hidereceive");
        $(".donate").removeClass("hidedonate");

    }
    else if(selectedradioid=="intendreceive")
    {
        $(".donate").addClass("hidedonate");
        $(".receive").removeClass("hidereceive");
    }  
});

$('input:radio[name="adm-usertype"]').change(function(){
    selectedradioid=$(this).attr('id');
    if(selectedradioid=="adm-corporate")
    {
        
        $(".ngo").addClass("adminhidengo");
        $(".others").addClass("adminhideothers");
        $(".company").removeClass("adminhidecompany");

    }
    else if(selectedradioid=="adm-ngo")
    {
        $(".company").addClass("adminhidecompany");
        $(".others").addClass("adminhideothers");
        $(".ngo").removeClass("adminhidengo");
    }
    else if(selectedradioid=="adm-others")
    {
        $(".company").addClass("adminhidecompany");
        $(".ngo").addClass("adminhidengo");
        $(".others").removeClass("adminhideothers");
    }  

});


$('.radio').click(function(){
    $('.radio').css({"background-color":"#ffffff","color":"black"});
    $(this).css({"background-color":"#9c4141","color":"#dddddd"});
    selectedusertypeid = $(this).children("input").attr("id");
    console.log(selectedusertypeid);
    if(selectedusertypeid=="corporate")
    {   
        $(".registercorporate").removeClass("hidecompany");
        $(".registerngo").addClass("hidengo");
        $(".registerothers").addClass("hideothers");
        $(".registerindividual").addClass("hideindividual");
        $(".registerindividual input,select").prop('required',false);
        $('#registercorporate select').prop('required',true);
        $("#registercorporate input").prop('required',true);
        $(".registerngo input,select").prop('required',false);
        $(".registerothers input,select").prop('required',false);
    }
    else if(selectedusertypeid=="ngo")
    {   

        $(".registercorporate").addClass("hidecompany");
        $(".registerothers").addClass("hideothers");
        $(".registerindividual").addClass("hideindividual");
        $(".registerngo").removeClass("hidengo");
        $(".registerindividual input,select").prop('required',false);
        $("#registercorporate input").prop('required',false);
        $("#registercorporate select").prop('required',false);
        $("#registerothers input").prop('required',false);
        $("#registerothers select").prop('required',false);
        $("#registerngo input").prop('required',true);
        $("#registerngo select").prop('required',true);

    }
    else if(selectedusertypeid=="individual")
    {
        $(".registercorporate").addClass("hidecompany");
        $(".registerngo").addClass("hidengo");
        $(".registerothers").addClass("hideothers");
        $(".registerindividual").removeClass("hideindividual");
        $(".registerindividual input,select").prop('required',true);
        $("#registercorporate input").prop('required',false);
        $("#registercorporate select").prop('required',false);
        $(".registerngo input,select").prop('required',false);
        $(".registerothers input,select").prop('required',false);
       
    }  
    else if(selectedusertypeid=="others")
    {
        $(".registercorporate").addClass("hidecompany");
        $(".registerngo").addClass("hidengo");
        $(".registerindividual").addClass("hideindividual");
        $(".registerothers").removeClass("hideothers");
        $(".registerindividual input,select").prop('required',false);
        $("#registercorporate select").prop('required',false);
        $("#registercorporate input").prop('required',false);
        
        $("#registerngo input").prop('required',false);
        $("#registerngo select").prop('required',false);
        $("#registerothers input").prop('required',true);
        $("#registerothers select").prop('required',true);
        // $(".registerngo input,select").prop('required',false);
    }  

})


//verify otp
var showotpdiv = 1;
$('#verifyotp').click(function(e){ 
    
    $.ajax({
       
        success:function(data){ 
                if(showotpdiv==1)
                {
                    $(".mobotp").removeClass("hideotp");
                    showotpdiv=0;  
                }
                else if(showotpdiv==0)
                {
                    $(".mobotp").addClass("hideotp");
                    showotpdiv=1;
                }
                
           
        } ,// end success
        error: function(){alert("failes");}
    }); // end ajax
}); //end click


// multi step form
//end multi step form

// clickable-row

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

function forgotPwd(){
    var email = $("#email").val();
    var email_pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if(email ==''){
        var msg='Please Enter email.';
        $('.msg').html("<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+ msg +"</div>");
        $('#email').focus();
        return false;
    }else if(email_pattern.test(email)==false){
        var msg='Invalid email';
        $('.msg').html("<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+ msg +"</div>");
        $('#email').focus();
        return false;
    }else {
        $('.msg').html("")
        return true;
    }
}

$("#inputEmail").click(function(){
    if(forgotPwd() == true){
        var form_data = $("#forgot-password-form").serialize();
        var csrfval = $('input[name="csrfmiddlewaretoken"]').val();
        $.ajax({
            url : "/forgotpassword/",
            type : "POST",
            data : form_data,
            headers: { 'X-CSRFToken': csrfval },
            success : function(data){
                if(data.success){
                    $('.msg').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+ data.success +"</div>");
                    $('#email').val("");
                }
            },
            error: function (error) {
                if(error.status==400){
                    $('.msg').html("<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>User with this email does not exists!</div>");
                    $('#email').val("");
                }
            }
        });
    }
});

$(document).ready(function(){
    $('#project-update').verticalTimeline({
        startLeft: false,
        alternate: true,
        animate: "fade",
        arrows: true
    });
});


$(document).ready(function() {
    // $('#project-update').timeline();
    // $.fn.editable.defaults.mode = 'inline';
    $(".adm-member").click(function(){
        var updateElementId = $(this).attr('id');
        $('#'+updateElementId).editable();
    });
});

$(document).on("click",".directorcross",function(){
    var removeelement =$(this).attr("data-parent");
    // current_element_no = removeelement.match(/\d+/); //find element to be deleted
    // total_elements =  $('div[name = "directors"]').length;
    $('#'+removeelement).remove();// delete element
    // for(var i=parseInt(current_element_no)+1; i<=total_elements;i++) // if didnt work then add current_element_no[0]
    // {
    //     $('#append_member_'+i).attr('id','append_member_'+(i-1));
    //     $('#d_name_'+i).attr('id','d_name_'+(i-1));
    //     $('#d_designation_'+i).attr('id','d_designation_'+(i-1));
    //     $('#d_emailid_'+i).attr('id','d_emailid_'+(i-1));
    //     $('#member_'+i).attr('data-parent','append_member_'+(i-1));
    //     $('#member_'+i).attr('id','member_'+(i-1));
    //     $('#d_mobileno_'+i).attr('id','d_mobileno_'+(i-1));
    //     $('#d_photo_'+i).attr('id','d_photo_1'+(i-1));
    //     $('#d_desc_'+i).attr('id','d_desc_1'+(i-1));
    // }
    // dir = dir-1;
});  

$(document).on("click",".csrcross",function(){
    var removeelement =$(this).attr("data-parent");
    current_element_no = removeelement.match(/\d+/); //find element to be deleted
    total_elements =  $('div[name = "csrmember"]').length;
    $('#'+removeelement).remove();// delete element
    for(var i=parseInt(current_element_no)+1; i<=total_elements;i++) // if didnt work then add current_element_no[0]
    {
        $('#append_csrmember_'+i).attr('id','append_csrmember_'+(i-1));
        $('#c_name_'+i).attr('id','c_name_'+(i-1));
        $('#c_designation_'+i).attr('id','c_designation_'+(i-1));
        $('#c_emailid_'+i).attr('id','c_emailid_'+(i-1));
        $('#csrmember_'+i).attr('data-parent','append_csrmember_'+(i-1));
        $('#csrmember_'+i).attr('id','csrmember_'+(i-1));
        $('#c_mobileno_'+i).attr('id','c_mobileno_'+(i-1));
        $('#c_photo_'+i).attr('id','c_photo_1'+(i-1));
        $('#c_desc_'+i).attr('id','c_desc_1'+(i-1));
    }
    csr = csr-1;
});

var dir=1;
$("#add-directors").click(function(){
    dir++;
    console.log(dir)
    clone_element = $("#append_member_1").clone();
    // $(clone_element).find('#d_name_1').attr('id','d_name_'+dir);
    // $(clone_element).find('#d_designation_1').attr('id','d_designation_'+dir);
    // $(clone_element).find('#d_emailid_1').attr('id','d_emailid_'+dir);
    // $(clone_element).find('#d_mobileno_1').attr('id','d_mobileno_'+dir);
    // $(clone_element).find('#d_photo_1').attr('id','d_photo_'+dir);
    // $(clone_element).find('#d_desc_1').attr('id','d_desc_'+dir);
    $(clone_element).find('#add-director-btn').hide();
    // clone_element.attr('id','append_member_'+dir);
    // $(clone_element).find('#member_1').attr('id','member_'+dir).attr('data-parent','append_member_'+dir);
    $(clone_element).find('.hidecross').removeClass('hidecross');
    $(clone_element).appendTo("#directors");
}); 

var csr=1;
$("#add-csrmem").click(function(){
    csr++;
    console.log(csr)
    clone_element = $("#append_csrmember_1").clone();
    $(clone_element).find('#c_name_1').attr('id','c_name_'+csr);
    $(clone_element).find('#c_designation_1').attr('id','c_designation_'+csr);
    $(clone_element).find('#c_emailid_1').attr('id','c_emailid_'+csr);
    $(clone_element).find('#c_mobileno_1').attr('id','c_mobileno_'+csr);
    $(clone_element).find('#c_photo_1').attr('id','c_photo_'+csr);
    $(clone_element).find('#c_desc_1').attr('id','c_desc_'+csr);
    $(clone_element).find('#add-csrmem-btn').hide();
    clone_element.attr('id','append_csrmember_'+csr);
    $(clone_element).find('#csrmember_1').attr('id','csrmember_'+csr).attr('data-parent','append_csrmember_'+csr);
    $(clone_element).find('.hidecsrcross').removeClass('hidecsrcross');
    $(clone_element).appendTo("#csr-committee");
});


var tru=0;
$("#add-trustees").click(function(){
    tru++; 
    $(" <div id='append_trustmember_"+tru+"'>\
            <div class='span12 first'>\
                <div class='form-group span3'>\
                    <label for='email'>Director Name</label>\
                    <input type='text' class='form-control' id=t-name-"+tru+">\
                </div>\
                <div class='form-group span3'>\
                    <label for='pwd'>Directors Designation:</label>\
                    <input type='text' class='form-control' id=t-designation-"+tru+">\
                </div>\
                <div class='form-group span3'>\
                    <label for='pwd'>Directors Email-id:</label>\
                    <input type='text' class='form-control' id=t-emailid-"+tru+">\
                </div>\
                <div class='form-group span3 m-rem-btn'>\
                    <a data-parent='#append_trustmember_"+tru+"' id='member_"+tru+"' class='icon icon-remove-sign icon-3x' aria-hidden='true'></a>\
                </div\
            </div>\
            <div class='span12 first'>\
                <div class='form-group span3'>\
                    <label for='email'>Mobile Number</label>\
                    <input type='text' class='form-control' id=t-mobileno-"+tru+">\
                </div>\
                <div class='form-group span3'>\
                    <label for='pwd'>Directors Photo:</label>\
                    <input type='file' class='form-control' id=t-photo-"+tru+">\
                </div>\
                <div class='form-group span3'>\
                    <label for='pwd'>Description:</label>\
                    <input type='text' class='form-control' id=t-desc-"+tru+">\
                </div>\
            </div>\
        </div>").appendTo("#trustees");
});


// var csr=0;
// $("#add-csrmem").click(function(){
//     csr++; 
//     $(" <div id='append_csrmember_"+csr+"' class='append_members'>\
//             <div class='span12 first'>\
//                 <div class='form-group span3'>\
//                     <label for='email'>Member Name</label>\
//                     <input type='text' class='form-control' id=c-name-"+csr+">\
//                 </div>\
//                 <div class='form-group span3'>\
//                     <label for='pwd'>Member Designation:</label>\
//                     <input type='text' class='form-control' id=c-designation-"+csr+">\
//                 </div>\
//                 <div class='form-group span3'>\
//                     <label for='pwd'>Member Email-id:</label>\
//                     <input type='text' class='form-control' id=c-emailid-"+csr+">\
//                 </div>\
//                 <div class='form-group span3 m-rem-btn'>\
//                     <a data-parent='#append_csrmember_"+csr+"' id='member_"+csr+"' class='icon icon-remove-sign icon-3x' aria-hidden='true'></a>\
//                 </div\
//             </div>\
//             <div class='span12 first'>\
//                 <div class='form-group span3'>\
//                     <label for='email'>Member Mobile Number</label>\
//                     <input type='text' class='form-control' id=c-mobileno-"+csr+">\
//                 </div>\
//                 <div class='form-group span3'>\
//                     <label for='pwd'>Members Photo:</label>\
//                     <input type='file' class='form-control' id=c-photo-"+csr+">\
//                 </div>\
//                 <div class='form-group span3'>\
//                     <label for='pwd'>Member Description:</label>\
//                     <input type='text' class='form-control' id=c-desc-"+csr+">\
//                 </div>\
//             </div>\
//         </div>").appendTo("#csr-committee");
// });

var cause=0;
$("#add-causes").click(function(){
    cause++;console.log(cause);

    $(" <div id='append_cause_"+cause+"' class='span12 first'>\
            <div class='span3'>\
                <div class='control-group'> \
                    <label for='email'>Cause Category</label>\
                    <input type='text' class='input-block-level' id=cause-"+cause+">\
                </div>\
            </div>\
            <div class='span3'>\
                <div class='control-group'> \
                    <label for='pwd'>Cause Sub-Category:</label>\
                    <input type='text' class='input-block-level' id=sub-cause-"+cause+">\
                </div>\
            </div>\
            <div class='span3'>\
                <div class='control-group'> \
                    <label for='pwd'>Cause Description:</label>\
                    <input type='text' class='input-block-level' id=cause-desc-"+cause+">\
                </div>\
            </div>\
            <div class='span3 m-rem-btn'>\
                <a data-parent='#append_cause_"+cause+"' id='member_"+cause+"' class='icon icon-remove-sign icon-3x' aria-hidden='true'></a>\
            </div>\
        </div>").appendTo("#causes");
});

// $('#append_cause').hide();
// $("#add-causes").click(function(){
//     cause++;console.log(cause);
//     $div = $('#append_cause').clone().prop('id',cause);
    
//     $($div).appendTo("#causes");
// });

var individual_c=0;
$("#add-individualcauses").click(function(){
    individual_c++; 
    $(" <div id='append_individualcause_"+individual_c+"' class='span12 first'>\
            <div class='form-group span3'>\
                <label for='email'>Cause Category</label>\
                <input type='text' class='form-control' id=individualcause-"+individual_c+">\
            </div>\
            <div class='form-group span3'>\
                <label for='pwd'>Cause Sub-Category:</label>\
                <input type='text' class='form-control' id=individualsub-cause-"+individual_c+">\
            </div>\
            <div class='form-group span3'>\
                <label for='pwd'>Cause Description:</label>\
                <input type='text' class='form-control' id=individualcause-desc-"+individual_c+">\
            </div>\
            <div class='form-group span3 m-rem-btn'>\
                <a data-parent='#append_individualcause_"+individual_c+"' id='member_"+individual_c+"' class='icon icon-remove-sign icon-3x' aria-hidden='true'></a>\
            </div\
        </div>").appendTo("#individual-causes");
});


var others_c=0;
$("#add-otherscauses").click(function(){
    others_c++; 
    $("<div id='append_individualcause_"+others_c+"' class='span12 first'>\
            <div class='form-group span3'>\
                <label for='email'>Cause Category</label>\
                <input type='text' class='form-control' id=otherscause-"+others_c+">\
            </div>\
            <div class='form-group span3'>\
                <label for='pwd'>Cause Sub-Category:</label>\
                <input type='text' class='form-control' id=otherssub-cause-"+others_c+">\
            </div>\
            <div class='form-group span3'>\
                <label for='pwd'>Cause Description:</label>\
                <input type='text' class='form-control' id=otherscause-desc-"+others_c+">\
            </div>\
            <div class='form-group span3 m-rem-btn'>\
                <a data-parent='#append_individualcause_"+others_c+"' id='member_"+others_c+"' class='icon icon-remove-sign icon-3x' aria-hidden='true'></a>\
            </div\
        </div>").appendTo("#others-causes");
});

// var kind=0;
// $("#btn-kind").click(function(){
//     kind++; 
//     $("<tr class='tdn'>\
//         <td>\
//             <input type='text' name='' placeholder='Enter Quantity'>\
//         </td>\
//         <td>\
//             <div class='styled-select slate'>\
//                 <select>\
//                     <option>Enter Kind type</option>\
//                     <option>option 1</option>\
//                     <option>option 2</option>\
//                     <option>option 3</option>\
//                     <option>option 4</option>\
//                 </select>\
//             </div>\  
//         </td>\
//         <td>\
//             <div class='styled-select slate'>\
//                 <select>\
//                     <option>Enter Sub-Kind type</option>\
//                     <option>option 1</option>\
//                     <option>option 2</option>\
//                     <option>option 3</option>\
//                     <option>option 4</option>\
//                 </select>\
//             </div>\  
//         </td>\
//         <td>\
//             <input type='text' name='' placeholder='kind specifics'>\
//         </td>\
//         <td>\
//             <div class='styled-select slate'>\
//                 <select>\
//                     <option>Enter Frequency</option>\
//                     <option>option 1</option>\
//                     <option>option 2</option>\
//                     <option>option 3</option>\
//                     <option>option 4</option>\
//                 </select>\
//             </div>\
//         </td>\
//         </tr>").appendTo("#kind-table tbody");
// });

var kind=1;
$("#btn-kind").click(function(){
    kind++; 
    $("<tr>\
            <td>\
                <input id=quantity"+kind+" type='text' name='' placeholder='Enter Quantity'>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=kindtype"+kind+">\
                    <option>Enter Kind type</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=kindsubtype"+kind+">\
                    <option>Enter Kind Sub-type</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
            <td>\
                <input id=kindspec"+kind+" type='text' name='' placeholder='Kind Specifics'>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=kindfreq"+kind+">\
                    <option>Enter Kind Frequency</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
        </tr>").appendTo("#kind-table tbody"); 
});

var time=1;
$("#btn-time").click(function(){
    time++; 
    $("<tr>\
            <td>\
                <input id=manhours"+time+" type='text' name='' placeholder='Number of Man/Hours'>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=timetype"+time+">\
                    <option>Enter Skill type</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=timesubtype"+time+">\
                    <option>Choose Sub-Skill</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
            <td>\
                <input id=timespec"+time+" type='text' name='' placeholder='Enter time specifics'>\
            </td>\
            <td>\
                <div class='styled-select slate'>\
                    <select id=timefreq"+time+">\
                    <option>Choose Time Frequency</option>\
                    <option>option 1</option>\
                    <option>option 2</option>\
                    <option>option 3</option>\
                    </select>\
                </div>\
            </td>\
        </tr>").appendTo("#time-table tbody"); 
});

// $('.faqhide').hide();
$('.mem_faqask').click(function(){
    $('.faqhide').show();
})
// var faq=5;
// $('#ask_question').keydown(function(e){
//     if(e.keyCode==13)
//     {   
//         $('.faqhide').hide();
//         console.log("enter is pressed");
//         enteredtext = $(this).val();
//         console.log(enteredtext);
//         $(" <div class='accordion-group'>\
//                 <div class='accordion-heading'>\
//                     <a class='accordion-toggle inactive' data-toggle='collapse' data-parent='#accordion2' href='#collapse"+faq+"'>\
//                         <span class='toggle_faq'><i class='icon-chevron-right'></i> </span>  <h3>  <strong>"+enteredtext+" </strong> </h3>\
//                     </a>\
//                 </div>\
//                 <div id='collapse"+faq+"' class='accordion-body collapse'>\
//                     <div class='accordion-inner'>\
//                         <p>No Answers Yet!!!\
//                         </p>\
//                     </div>\
//                 </div>\
//             </div>").appendTo("#accordion2")
//         $('.faqhide').val('');    
//         faq++;
//     }
// });




// $('.mem_faqask').click(function(){
//     $(" <div class='accordion-group'>\
//             <div class='accordion-heading'>\
//                 <a class='accordion-toggle active' data-toggle='collapse' data-parent='#accordion2' href='#collapseFive'>\
//                      <span class='toggle_faq'><i class='icon-chevron-right'></i> </span>  <h3>  <strong>project Questions 1? </strong> </h3>\
//                 </a>\
//             </div>\
//             <div id='collapseFive' class='accordion-body collapse in'>\
//                 <div class='accordion-inner'>\
//                     <p>  </p>\
//                 </div>\
//             </div>\
//         </div>").appendTo("#accordion2")


// console.log("faq1");

// // <div class='accordion-group'>\
// //     <div class='accordion-heading'>\
// //         <a class='accordion-toggle active' data-toggle='collapse' data-parent='#accordion2' href='#collapseOne'>\
// //              <span class='toggle_faq'><i class='icon-chevron-right'></i> </span>  <h3>  <strong>project Questions 1? </strong> </h3>\
// //         </a>\
// //     </div>\
// //     <div id="collapseOne" class="accordion-body collapse in">\
// //         <div class="accordion-inner">\
// //             <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit.\
// //             Proin aliquam mattis neque, vehicula congue nisl rhoncus vitae.\
// //             Integer congue, dolor a fringilla ultrices, mauris mauris eleifend orci,\
// //             ac commodo nisi lacus sed felis. Cras nec ipsum id nunc rutrum consectetur.\
// //             Lorem ipsum dolor sit amet, consectetur adipiscing elit.\
// //             Proin aliquam mattis neque, vehicula congue nisl rhoncus vitae.\
// //             Integer congue, dolor a fringilla ultrices, mauris mauris eleifend orci,\
// //             ac commodo nisi lacus sed felis. Cras nec ipsum id nunc rutrum consectetur.  </p>\
// //         </div>\
// //     </div>\
// // </div>


// });

$(document).ready(function(){
    $('#btn-makerequest').click(function(){
        // $('#generate-request').remove();
    });
});

// $('.menuzord-menu li').removeClass('active');
// $('ul.menuzord-menu li ').click(function(){
//     alert(this);    
//     $(this).parent().addClass('active');
// });



function setNavigation() {
    console.log("navigation test");
    $('.menuzord-menu li').removeClass('active');
    var path = window.location.pathname;

    $("ul.menuzord-menu li a").each(function() {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            // $(this).parent('li').first().addClass('active');
            $(this).closest('.active_menuitem').addClass('active');
        }
    });
    
}


$(function(){
    setNavigation();
});



$(document).ready(function() {
$(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below
    $(this).removeClass("btn-default").addClass("btn-primary");   
});

$(".btn-pref .btn-basic").click(function () {
    $(".btn-pref .story").removeClass("btn-default").addClass("btn-primary");
    $(".btn-pref .basics").removeClass("btn-primary").addClass("btn-default");
});
$(".btn-pref .btn-story-bck").click(function () {
    $(".btn-pref .basics").removeClass("btn-default").addClass("btn-primary");
    $(".btn-pref .story").removeClass("btn-primary").addClass("btn-default");
});

});


// $(document).ready(function(){
//     $(window).scroll(function(){
//         console.log("scroll in all projects");
//         lazyload.load();
        
//     });
// });

// Instantiate the Bloodhound suggestion engine
// Instantiate the Bloodhound suggestion engine
var movies = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        // url: 'http://api.themoviedb.org/3/search/movie?query=%QUERY&api_key=f22e6ce68f5e5002e71c20bcba477e7d',
        url: 'https://api.themoviedb.org/3/movie/550?api_key=3cf1ce96036494dd0e500df1a09f54c9',
        filter: function (movies) {
            // Map the remote source JSON array to a JavaScript object array
            return $.map(movies, function (movie) {
                return {
                    value: movies.original_title
                };
            });
        }
    }
});


// Initialize the Bloodhound suggestion engine
movies.initialize();

// Instantiate the Typeahead UI
$('.typeahead').typeahead(null, {
    displayKey: 'value',
    source: movies.ttAdapter()
});

$('#search-txt').keydown(function(e){
    if(e.keyCode==13)
    {   
        console.log("enter is pressed");
        enteredtext = $(this).val();
        console.log(enteredtext);
        window.location.href = "/search/result/"+enteredtext;
    }
});

// token field along with type-ahead
var engine = new Bloodhound({
        local: [ {value: 'jay'}, 
               {value: 'vijay'}, 
               {value: 'ajay'}, 
               {value: 'shiv'}, 
               {value: 'vishnu'}, 
               {value: 'ram'}, 
               {value: 'shyam'}, 
               {value: 'oankar'}, 
               {value: 'ravi'} ] ,

        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        } ,

        queryTokenizer: Bloodhound.tokenizers.whitespace
});

engine.initialize();

$('#tag-contributors').tokenfield({
  typeahead: [null, { source: engine.ttAdapter() }]
});


//$('.tokenfield').tokenfield();
// end token field along with type-ahead

$('#adm-tag-contributors').tokenfield({
  typeahead: [null, { source: engine.ttAdapter() }]
});
//$('.tokenfield').tokenfield();                                                                                                                                                                                                                                                                                                                              
$('.m-adjust .count').hide();
$('.m-adjust .daterange').hide();

$('input:radio[name="duration"]').change(function(){
    checkedid = $(this).attr('id');
    if(checkedid=="noofdays")
    {
        $('.m-adjust .count').show();
        $('.m-adjust .daterange').hide();
    }
    else
    {   
        $('.m-adjust .count').hide();
        $('.m-adjust .daterange').show();
    }
})



function readURL(input, target) 
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var image_target = $(target);
        reader.onload = function (e) {
            image_target.attr('src', e.target.result).show();
        };
        reader.readAsDataURL(input.files[0]);
     }
}   
$("#patient_pic").on("change",function(){
    readURL(this, "#preview_image")
});

// $('.project_image').on("change",function(){
//     var filevalue = $('input:file[name="projectimage"]').val();
//     console.log(filevalue);
//     filevalue = filevalue.substring(12);
//     console.log(filevalue);
//     $('#image_value').empty();
//     $('#image_value').append("<p>"+filevalue+"</p>")
// })

$('.project_video').on("change",function(){
    var filevalue = $('input:file[name="projectvideo"]').val();
    console.log(filevalue);
    filevalue = filevalue.substring(12);
    console.log(filevalue);
    $('#video_value').empty();
    $('#video_value').append("<p>"+filevalue+"</p>")
})

$('.activity_image').on("change",function(){
    var filevalue = $(this).val();
    filevalue = filevalue.substring(12);
    $('.image_value').empty();
    $('.image_value').append("<p>"+filevalue+"</p>")
})

$(document).on("change",".project_image",function(){
    var filevalue = $(this).val();
    console.log(filevalue);
    filevalue = filevalue.substring(12);
    $('#image_value').empty();
    $('#image_value').append("<p>"+filevalue+"</p>")
})


$('.activity_video').on("change",function(){
    var filevalue = $(this).val();
    filevalue = filevalue.substring(12);
    $('.video_value').empty();
    $('.video_value').append("<p>"+filevalue+"</p>")
})

/*for file input field*/
!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
/*end file input field*/


// $(function () {
//   $('#signup').parsley().on('field:validated', function() {
//     var ok = $('.parsley-error').length === 0;
//     console.log("ok")
//     console.log(ok)
//     $('.bs-callout-info').toggleClass('hidden', !ok);
//     $('.bs-callout-warning').toggleClass('hidden', ok);
//   })
//   .on('form:submit', function(e) {
//     e.preventDefault();
//     var form = $(this);
//     // if(form.parsley().isValid())
//     // {
//     //     return true; 
//     //     console.log("validation successful")    
//     // }
//     alert('Not Valid')
//     return true;
//   });
// });


$(function () {
  $('#member-others').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    console.log("ok")
    console.log(ok)
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })
  .on('form:submit', function(e) {
    e.preventDefault();
    var form = $(this);
    if(form.parsley().isValid())
    {
        return true; 
        console.log("validation successful")    
    }
    console.log('validation not successful')
    return false;
  });
});

$(function () {
  $('#corporate').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    console.log("ok")
    console.log(ok)
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })
  .on('form:submit', function(e) {
    e.preventDefault();
    var form = $(this);
    if(form.parsley().isValid())
    {
        return true; 
        console.log("validation successful")    
    }
    alert('validation not successful')
    return false;
  });
});

$(function () {
  $('#member_ngo').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    console.log("ok")
    console.log(ok)
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })
  .on('form:submit', function(e) {
    e.preventDefault();
    var form = $(this);
    if(form.parsley().isValid())
    {
        return true; 
        console.log("validation successful")    
    }
    alert('validation not successful')
    return false;
  });
});

$(function () {
  $('#mem-individual').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    console.log("ok")
    console.log(ok)
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })
  .on('form:submit', function(e) {
    e.preventDefault();
    var form = $(this);
    if(form.parsley().isValid())
    {
        return true; 
        console.log("validation successful")    
    }
    alert('validation not successful')
    return false;
  });
});




$('#start-project').parsley({
    validators: {
        money: function () {
            return {
                validate: function (value, requirements) {
                    alert('money');
                    return false;
                },
                priority: 32
            }
        }
    },
    messages: {
        money: 'my validator failed'
    }
});



// $(function () {
//   $('#start-project').parsley().on('field:validated', function() {


//     Parsley.addValidator('money', {
//         validateString: function(value) {
//         return false;
//         },
//         messages: {
//         en: 'Cannot Add money Frequency without money',
//         }
//         });



//   })
//   .on('form:submit', function(e) {
//     e.preventDefault();
//     alert("parsley method");
//     var form = $(this);
//     if(form.parsley().isValid())
//     {
//         return true; 
//     }
//     $('.bs-callout-info').toggleClass('hidden', 'false');
//     return false;
//   });
// });

//project create page review functionality

$('#preview_div').click(function(){

console.log("preview clicked");
var project_image_path = $('input[name="project_image"]').val();
var project_title = $('input[name="title"]').val();
var project_short_desc = $('textarea[name="short_desc"]').val();
var project_cause = $('input[name="project_cause"]').val();
var project_subcause = $('input[name="project_subcause"]').val();
var project_cause_specifics = $('input[name="project_cause_specifics"]').val();
var location = $('input[name="location"]').val();
var duration = $('input[name="duration"]:checked').val();
var no_of_count = $('input[name="no_of_days"]').val();
var start_date = $('input[name="start_date"]').val();
var end_date = $('input[name="end_date"]').val();
var long_desc =$('textarea[name="long_desc"]').val();

var money_detail = $('input[name="money"]').val();
var money_frequency = $('select[name="money_frequency"] option:selected').text();
var money_frequency_val = $('select[name="money_frequency"] option:selected').val();

var kind_quantity = $('input[name="kind-0-quantity"]').val();

var kind_type = $('select[name="kind-0-kind_type"] option:selected').text();
var kind_type_val = $('select[name="kind-0-ktype"] option:selected').val();
var kind_subtype = $('select[name="kind-0-ksubtype"] option:selected').text();
var kind_subtype_val = $('select[name="kind-0-ksubtype"] option:selected').val();
var kind_specifics = $('input[name="kind-0-specifics"]').val();
var kind_frequency = $('select[name="kind-0-frequency"] option:selected').text();
var kind_frequency_val = $('select[name="kind-0-frequency"] option:selected').val();

var time_manhours = $('input[name="time-0-manhours"]').val();
var time_skill_type = $('select[name="time-0-skilltype"] option:selected').text();
var time_skill_type_val = $('select[name="time-0-skilltype"] option:selected').val();
var time_skill_subtype = $('select[name="time-0-skillsubtype"] option:selected').text();
var time_skill_subtype_val = $('select[name="time-0-skillsubtype"] option:selected').val();
var time_specifics = $('input[name="time-0-specifics"]').val();
var time_frequency = $('select[name="time-0-frequency"] option:selected').text();
var time_frequency_val = $('select[name="time-0-frequency"] option:selected').val();
var video_link = $('input[name="project_video_link"]').val()

if(duration == "True")

{
    
    if(no_of_count!="")
    {
        $('#project-duration').empty().append("Number of Days :  "+no_of_count);
    }
}
else
{
    if(start_date!="" && end_date!="")
    {
        $('#project-duration').empty().append("Start Date : "+ start_date);
        $('#project-duration').append("   End Date : "+ end_date);
    }
}

if(video_link != "")
{
    $('#video_link').empty().append("Project Video Link :")
    $("#project-video-link").attr("href", video_link);
    $("#project-video-link").append(video_link)
}


if(location != "")
{
    $('#project-location').empty().append("Location : "+ location);
}

if(money_detail!="")
{
    $('#moneydetail').empty().append(money_detail); 
    $('#moneydetail').append(" INR");
    if(money_frequency_val!="")
    {
        $('#moneydetail').append(money_frequency);        
    }
    
}

if(kind_quantity!="")
{
    $('#kinddetail').empty().append(kind_quantity); 
    $('#kinddetail').append(" ");
    if(kind_type_val!="")
    {
        $('#kinddetail').append(kind_type);
        $('#kinddetail').append(" ");
    }
    if(kind_subtype_val!="")
    {
        $('#kinddetail').append(kind_subtype);
        $('#kinddetail').append(" ");
    }
    if(kind_specifics!="")
    {
        $('#kinddetail').append("[");
        $('#kinddetail').append(kind_specifics);
        $('#kinddetail').append("]");
        $('#kinddetail').append(" ");
    }
    if(kind_subtype_val!="")
    {
        $('#kinddetail').append(kind_subtype);
        $('#kinddetail').append(" ");
    }
}

if(time_manhours!="")
{
    
    $('#timedetail').empty().append(time_manhours); 
    $('#timedetail').append(" ");
    if(time_skill_type_val!="")
    {
        $('#timedetail').append(time_skill_type);
        $('#timedetail').append(" ");
    }
    if(time_skill_subtype_val!="")
    {
        $('#timedetail').append(time_skill_subtype);
        $('#timedetail').append(" ");
    }
    if(time_specifics!="")
    {
        $('#timedetail').append("[");
        $('#timedetail').append(time_specifics);
        $('#timedetail').append("]");
        $('#timedetail').append(" ");
    }
    if(time_frequency_val!="")
    {
        $('#timedetail').append(time_frequency);
        $('#timedetail').append(" ");
    }

}


if(project_title!="")
{
    $('#project-title').empty().append(project_title);    
}
if(project_short_desc!="")
{
    $('#short-desc').empty().append(project_short_desc);    
}
// if(project_cause!="")
// {
//     $('#short-desc').empty().append(project_cause);   
// }
// if(project_subcause!="")
// {
//    $('#short-desc').empty().append(project_subcause);
// }
// if(project_cause_specifics!="")
// {
//     $('#short-desc').empty().append(project_cause_specifics);   
// }
// if(location!="")
// {
//     $('#short-desc').empty().append(location);
// }
// if(duration!="")
// {
//     $('#short-desc').empty().append(duration);  
// }
// if(no_of_count!="")
// {
//     $('#short-desc').empty().append(no_of_count);    
// }
// if(start_date!="")
// {
//     $('#short-desc').empty().append(start_date);    
// }
// if(end_date!="")
// {
//     $('#short-desc').empty().append(end_date);    
// }
if(long_desc!="")
{
    $('#long-desc').empty().append(long_desc);    
}


});



$('#allphotos').click(function(){
    $('.allphotos').show();
    $('.ownphotos').hide();
    $('.allcausesphotos').hide();
    $('.alleventphotos').hide();
})

$('#ownphotos').click(function(){
    $('.allphotos').hide();
    $('.ownphotos').show();
    $('.allcausesphotos').hide();
    $('.alleventphotos').hide();
})

$('#allphotoscauses').click(function(){
    $('.allphotos').hide();
    $('.ownphotos').hide();
    $('.allcausesphotos').show();
    $('.alleventphotos').hide();
})

$('#allphotosevents').click(function(){
    $('.allphotos').hide();
    $('.ownphotos').hide();
    $('.allcausesphotos').hide();
    $('.alleventphotos').show();
})


$('.allvideos').show();
$('.allcausesvideos').show();
$('.alleventvideos').show();

$('#allvideos').click(function(){
    $('.allvideos').show();
    $('.ownvideos').show();
    $('.allcausesvideos').show();
    $('.alleventvideos').show();
})

$('#ownvideos').click(function(){
    $('.allvideos').hide();
    $('.ownvideos').show();
    $('.allcausesvideos').hide();
    $('.alleventvideos').hide();
})

$('#allvideoscauses').click(function(){
    $('.allvideos').hide();
    $('.ownvideos').hide();
    $('.allcausesvideos').show();
    $('.alleventvideos').hide();
})

$('#allvideosevents').click(function(){
    $('.allvideos').hide();
    $('.ownvideos').hide();
    $('.allcausesvideos').hide();
    $('.alleventvideos').show();
})


$('input[name="findetail-0-financialdate"]').change(function(){
    alert("test date");
    datevalue = $(this).val();
    $('input[name="findetail-1-financialdate"]').attr('value',datevalue);
    $('input[name="findetail-2-financialdate"]').attr('value',datevalue);

})


window.Parsley.addValidator('imagevalidation', {
    validateString: function(value) {
        image_ext = value.split('.')[1] ;
        required_image_ext = ['JPEG','JPG','PNG','jpeg','jpg','png'] ;
        
        for(image=0;image<required_image_ext.length;image++)
            {
                if(image_ext==required_image_ext[image])
                {
                    return true
                }
            } 
        return false
        
    },
    messages: {
        en: 'Supported Image Format are jpeg,jpg and png'
    }
});



// for filling financial date in ngo step 4 financial details
$("#project_financialdate").datepicker({
    
    onSelect: function(dateText, inst) {
        
        alert('on select triggered');
        
    }
});




// end filling financial date 


$('.counter').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 1000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  
});





$('.communityreceipients').hide();
$('.communityprojects').hide();

$('input[name="entity"]').change(function(){
    checkedelementvalue = $(this).val();
    if(checkedelementvalue=="contributors")
    {
        $('.communitycontributors').show();
        $('.communityreceipients').hide();
        $('.communityprojects').hide();
    }
    else if(checkedelementvalue=="recipients")
    {
        $('.communitycontributors').hide();
        $('.communityreceipients').show();
        $('.communityprojects').hide();
    }else if(checkedelementvalue=="projects")
    {
        $('.communitycontributors').hide();
        $('.communityreceipients').hide();
        $('.communityprojects').show();
    }
});

/*works with errors */
$('.datepicker').datepicker({
});

$('.year').datepicker({
        autoclose: true,
        format: "   yyyy",
        viewMode: "years", 
        minViewMode: "years",
        startDate: '2014',
        endDate: new Date(),
});

//signup form validation
// $('#signup').parsley();


