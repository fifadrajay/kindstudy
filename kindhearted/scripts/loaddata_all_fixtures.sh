#!/bin/bash
# Load all fixtures

PS3='Please enter your choice: '
options=("All" "frontend" "Quit")
select opt in "${options[@]}"
do
    case $opt in
    	"All")
			echo "Loading NGOFormOfOrganization data ==>"
			python ../manage.py loaddata ../fixtures/ngoformoforganization.json
			echo "Loading OTHERSFormOfOrganization data ==>"
			python ../manage.py loaddata ../fixtures/othersformoforganization.json
			echo "Loading CorporateCSRApplicability data ==>"
			python ../manage.py loaddata ../fixtures/corporatecsrapplicability.json
			echo "Loading OrganizationPurpose data ==>"
			python ../manage.py loaddata ../fixtures/organizationpurpose.json
			echo "Loading Location data ==>"
			python ../manage.py loaddata ../fixtures/location.json
			echo "Loading PersonalSkills data ==>"
			python ../manage.py loaddata ../fixtures/personalskills.json
			echo "Loading CauseCategory data ==>"
			python ../manage.py loaddata ../fixtures/causecategory.json
			echo "Loading Cause data ==>"
			python ../manage.py loaddata ../fixtures/cause.json
			echo "Loading SubCause data ==>"
			python ../manage.py loaddata ../fixtures/subcause.json
			
			# echo "Loading reciepent data ==>"
			# python ../manage.py loaddata ../fixtures/reciepent.json
			;;
		"frontend")
			echo "Loading location data ==>"
			python ../manage.py loaddata ../fixtures/frontend.json
			;;
		"Quit")
			break
			;;
        *) echo invalid option;;
    esac
done
