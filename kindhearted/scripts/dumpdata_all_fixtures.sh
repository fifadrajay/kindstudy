#!/bin/bash
# Load all fixtures
PS3='Please enter your choice: '
options=("All" "frontend" "Quit")
select opt in "${options[@]}"
do
    case $opt in
    	"All")
			# echo "Dumping UserProfile data ==>"
			# python ../manage.py dumpdata frontend > ../fixtures/frontend.json
			# echo "Dumping cause data ==>"
			# python ../manage.py dumpdata cause > ../fixtures/cause.json
			# echo "Dumping CauseCategory data ==>"
			python ../manage.py dumpdata frontend.NGOFormOfOrganization > ../fixtures/ngoformoforganization.json
			echo "Dumping NGOFormOfOrganization data ==>"
			python ../manage.py dumpdata frontend.OTHERSFormOfOrganization > ../fixtures/othersformoforganization.json
			echo "Dumping OTHERSFormOfOrganization data ==>"
			python ../manage.py dumpdata frontend.CorporateCSRApplicability > ../fixtures/corporatecsrapplicability.json
			echo "Dumping CorporateCSRApplicability data ==>"
			python ../manage.py dumpdata reciepent.OrganizationPurpose > ../fixtures/organizationpurpose.json
			echo "Dumping OrganizationPurpose data ==>"
			python ../manage.py dumpdata location.Location > ../fixtures/location.json
			echo "Dumping location data ==>"
			python ../manage.py dumpdata reciepent.PersonalSkills > ../fixtures/personalskills.json
			echo "Dumping PersonalSkills data ==>"

			python ../manage.py dumpdata cause.CauseCategory > ../fixtures/causecategory.json
			echo "Dumping Cause data ==>"
			python ../manage.py dumpdata cause.Cause > ../fixtures/cause.json
			echo "Dumping SubCause data ==>"
			python ../manage.py dumpdata cause.SubCause > ../fixtures/subcause.json
			# echo "Dumping Event data ==>"
			# python ../manage.py dumpdata cause.Event > ../fixtures/event.json
			# echo "Dumping PhotoGallery data ==>"
			# python ../manage.py dumpdata cause.PhotoGallery > ../fixtures/photo.json
			# echo "Dumping VideoGallery data ==>"
			# python ../manage.py dumpdata cause.VideoGallery > ../fixtures/video.json
			# echo "Dumping blog data ==>"
			# python ../manage.py dumpdata blog > ../fixtures/blog.json
			# echo "Dumping Blog data ==>"
			# python ../manage.py dumpdata blog.Blog > ../fixtures/blog.json
			# echo "Dumping BlogComment data ==>"
			# python ../manage.py dumpdata blog.BlogComment > ../fixtures/blogcomment.json
			# echo "Dumping contribution data ==>"
			# python ../manage.py dumpdata contribution  > ../fixtures/contribution.json
			# echo "Dumping MoneyFrequency data ==>"
			# python ../manage.py dumpdata contribution.MoneyFrequency  > ../fixtures/money_frequency.json
			# echo "Dumping KindFrequency data ==>"
			# python ../manage.py dumpdata contribution.KindFrequency  > ../fixtures/kind_frequency.json
			# echo "Dumping KindType data ==>"
			# python ../manage.py dumpdata contribution.KindType  > ../fixtures/kind_type.json
			# echo "Dumping KindSubType data ==>"
			# python ../manage.py dumpdata contribution.KindSubType  > ../fixtures/kind_subtype.json
			# echo "Dumping TimeSkillType data ==>"
			# python ../manage.py dumpdata contribution.TimeSkillType  > ../fixtures/time_skilltype.json
			# echo "Dumping TimeSkillSubType data ==>"
			# python ../manage.py dumpdata contribution.TimeSkillSubType  > ../fixtures/time_subskilltype.json
			# echo "Dumping Project data ==>"
			# python ../manage.py dumpdata project > ../fixtures/project.json
			# echo "Dumping Project data ==>"
			# python ../manage.py dumpdata project.Project  > ../fixtures/project.json
			# echo "Dumping ProjectFaq data ==>"
			# python ../manage.py dumpdata project.ProjectFaq  > ../fixtures/project_faq.json
			# echo "Dumping ProjectUpdates data ==>"
			# python ../manage.py dumpdata project.ProjectUpdates  > ../fixtures/project_updates.json
			# echo "Dumping ProjectComment data ==>"
			# python ../manage.py dumpdata project.ProjectComment  > ../fixtures/project_comment.json
			# echo "Dumping reciepent data ==>"
			# python ../manage.py dumpdata reciepent  > ../fixtures/reciepent.json
			# echo "Dumping CorporateBasic data ==>"
			# python ../manage.py dumpdata reciepent.CorporateBasic  > ../fixtures/corporate_basic.json
			# echo "Dumping CorporateOfficeBearers data ==>"
			# python ../manage.py dumpdata reciepent.CorporateOfficeBearers  > ../fixtures/corporate_officebearers.json
			# echo "Dumping IndividualDetails data ==>"
			# python ../manage.py dumpdata reciepent.IndividualDetails  > ../fixtures/individual_details.json
			# echo "Dumping OthersDetails data ==>"
			# python ../manage.py dumpdata reciepent.OthersDetails  > ../fixtures/others_details.json
			# echo "Dumping CsrApplicability data ==>"
			# python ../manage.py dumpdata reciepent.CsrApplicability  > ../fixtures/csr_applicability.json
			# echo "Dumping OthersFormOfOrganisation data ==>"
			# python ../manage.py dumpdata reciepent.OthersFormOfOrganisation  > ../fixtures/others_form_of_organisation.json
			;;
		"frontend")
			echo "Dumping frontend data ==>"
			python ../manage.py dumpdata frontend > ../fixtures/frontend.json
			;;
		"Quit")
			break
			;;
        *) echo invalid option;;
    esac
done
