from django.contrib import admin

from .models import *

admin.site.register(ContributionCategory)
admin.site.register(KindContribution)
admin.site.register(TimeContribution)
admin.site.register(Contribution)
admin.site.register(ContributionComment)
admin.site.register(GiveKind)
admin.site.register(GiveTime)
admin.site.register(IntendToGive)
admin.site.register(MoneyFrequency)
admin.site.register(KindType)
admin.site.register(KindSubType)
admin.site.register(KindFrequency)
admin.site.register(TimeSkillType)
admin.site.register(TimeSkillSubType)
admin.site.register(TimeFrequency)


