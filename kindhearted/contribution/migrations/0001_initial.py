# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-08-04 11:16
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cause', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contributed_on', models.DateField(blank=True, default=django.utils.timezone.now, null=True)),
                ('confirmed', models.BooleanField(default=False)),
                ('confirmed_on', models.DateField(blank=True, default=django.utils.timezone.now, null=True)),
                ('contribution_for', models.CharField(choices=[('1', 'PROJECT'), ('2', 'REQUEST')], default=1, max_length=20)),
                ('project', models.CharField(blank=True, max_length=255, null=True)),
                ('money', models.IntegerField(blank=True, null=True)),
                ('status', models.CharField(choices=[('1', 'Active'), ('2', 'Completed')], default='1', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ContributionCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ContributionComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commented_on', models.DateTimeField(verbose_name=datetime.datetime(2017, 8, 4, 11, 16, 37, 724422))),
                ('comments', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='GiveKind',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('specifics', models.CharField(blank=True, max_length=255, null=True)),
                ('photo', models.FileField(blank=True, null=True, upload_to='kind')),
                ('photo_tag', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='GiveTime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manhours', models.IntegerField(blank=True, null=True)),
                ('specifics', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='IntendToGive',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location', models.CharField(blank=True, max_length=100, null=True)),
                ('cause_description', models.CharField(blank=True, max_length=500, null=True)),
                ('money', models.CharField(blank=True, max_length=100, null=True)),
                ('project_desc', models.TextField(blank=True, null=True)),
                ('comment', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='KindContribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('specifics', models.CharField(blank=True, max_length=255, null=True)),
                ('photo', models.FileField(blank=True, null=True, upload_to='kind')),
                ('photo_tag', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='KindFrequency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='KindSubType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='KindType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='MoneyFrequency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Pledge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contributed_on', models.DateTimeField(verbose_name=datetime.datetime(2017, 8, 4, 11, 16, 37, 714438))),
                ('confirmed', models.BooleanField(default=False)),
                ('confirmed_on', models.DateTimeField(verbose_name=datetime.datetime(2017, 8, 4, 11, 16, 37, 714554))),
                ('is_fulfilled', models.BooleanField(default=False)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contribution.ContributionCategory')),
                ('cause', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cause.Cause')),
            ],
        ),
        migrations.CreateModel(
            name='TimeContribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manhours', models.IntegerField(blank=True, null=True)),
                ('specifics', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TimeFrequency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TimeSkillSubType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TimeSkillType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='timecontribution',
            name='frequency',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contribution.TimeFrequency'),
        ),
        migrations.AddField(
            model_name='timecontribution',
            name='skillsubtype',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contribution.TimeSkillSubType'),
        ),
        migrations.AddField(
            model_name='timecontribution',
            name='skilltype',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contribution.TimeSkillType'),
        ),
        migrations.AddField(
            model_name='timecontribution',
            name='time',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contribution.Contribution'),
        ),
    ]
