from __future__ import unicode_literals

from datetime import datetime
from django.db import models
from cause.models import Cause,SubCause,Community
from frontend.models import UserProfile
from django.utils import timezone

STATUS_CHOICE = [('1','Active'),('2','Completed')]
CONTRIBUTION_FOR = [('1','PROJECT'),('2','REQUEST')]

class ContributionCategory(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

class Pledge(models.Model):
    cause = models.ForeignKey(Cause)
    category = models.ForeignKey(ContributionCategory)
    contributed_by = models.ForeignKey(UserProfile)
    contributed_on = models.DateTimeField(datetime.now())
    confirmed = models.BooleanField(default=False)
    confirmed_by = models.ForeignKey(UserProfile,related_name="confirmedby")
    confirmed_on = models.DateTimeField(datetime.now())
    is_fulfilled = models.BooleanField(default=False)

    def __unicode__(self):
        return "Pledged by %s for %s"%(self.contributed_by,self.cause)


class MoneyFrequency(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class KindType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class KindSubType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class KindFrequency(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class TimeSkillType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class TimeSkillSubType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)

class TimeFrequency(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)



class IntendToGive(models.Model):
    user = models.ForeignKey(UserProfile)
    location = models.CharField(max_length=100,blank=True,null=True)
    community = models.ForeignKey(Community,blank=True,null=True)
    cause = models.ForeignKey(Cause, blank=True,null=True)
    cause_subcategory = models.ForeignKey(SubCause, blank=True,null=True)
    cause_description = models.CharField(max_length=500,blank=True,null=True)
    money = models.CharField(max_length=100,blank=True,null=True)
    money_frequency = models.ForeignKey(MoneyFrequency,blank=True,null=True)
    project_desc = models.TextField(blank=True,null=True)
    comment = models.TextField(blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.id)


class GiveKind(models.Model):
    kind = models.ForeignKey(IntendToGive,blank=True,null=True)
    quantity = models.IntegerField(blank=True,null=True)
    ktype = models.ForeignKey(KindType,blank=True,null=True)
    ksubtype = models.ForeignKey(KindSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(KindFrequency,blank=True,null=True)
    photo = models.FileField(upload_to="kind",blank=True,null=True)
    photo_tag = models.CharField(max_length=100,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.kind.id)

class GiveTime(models.Model):
    time = models.ForeignKey(IntendToGive,blank=True,null=True)
    manhours = models.IntegerField(blank=True,null=True)
    skilltype = models.ForeignKey(TimeSkillType,blank=True,null=True)
    skillsubtype = models.ForeignKey(TimeSkillSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(TimeFrequency,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.skilltype)     


class Contribution(models.Model): # diffrent from IntendToDonate , Contribution that actually happened
    request_id = models.ForeignKey('reciepent.IntendToReceive',blank=True,null=True)
    project_id = models.ForeignKey('project.Project',blank=True,null=True)
    pledge_id = models.ForeignKey(IntendToGive,blank=True,null=True)
    cause = models.ForeignKey(Cause,blank=True,null=True)
    category = models.ForeignKey(ContributionCategory,blank=True,null=True) #request subcategory
    contributed_by = models.ForeignKey(UserProfile,related_name="contributed_by")
    contributed_on = models.DateField(default=timezone.now,blank=True,null=True)
    confirmed = models.BooleanField(default=False)
    confirmed_by = models.ForeignKey(UserProfile,blank=True,null=True,related_name="confirmed_by")
    confirmed_on = models.DateField(blank=True,null=True,default=timezone.now)
    contribution_for = models.CharField(choices=CONTRIBUTION_FOR,max_length=20,default=1) #true for project ,false for request_id
    project = models.CharField(max_length=255,blank=True,null=True)
    money = models.IntegerField(blank=True,null=True)
    money_frequency = models.ForeignKey(MoneyFrequency,blank=True,null=True)
    status = models.CharField(max_length=255,choices=STATUS_CHOICE,default='1')
    

    def __unicode__(self):
        return "Contribution by %s To %s" % (self.contributed_by, self.project_id)

class ContributionComment(models.Model):
    contribution = models.ForeignKey(Contribution)
    commented_by = models.ForeignKey(UserProfile)
    commented_on = models.DateTimeField(datetime.now())
    comments = models.TextField()

    def __unicode__(self):
        return "%s 's comment" % self.commented_by

class KindContribution(models.Model):
    kind = models.ForeignKey(Contribution,blank=True,null=True)
    quantity = models.IntegerField(blank=True,null=True)
    ktype = models.ForeignKey(KindType,blank=True,null=True)
    ksubtype = models.ForeignKey(KindSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(KindFrequency,blank=True,null=True)
    photo = models.FileField(upload_to="kind",blank=True,null=True)
    photo_tag = models.CharField(max_length=100,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.kind)


class TimeContribution(models.Model):
    time = models.ForeignKey(Contribution,blank=True,null=True)
    manhours = models.IntegerField(blank=True,null=True)
    skilltype = models.ForeignKey(TimeSkillType,blank=True,null=True)
    skillsubtype = models.ForeignKey(TimeSkillSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(TimeFrequency,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.time)


