from django import forms
from django.forms import ModelForm
from contribution.models import IntendToGive,GiveKind,GiveTime,Contribution,KindContribution,TimeContribution
from django.forms import ModelForm ,inlineformset_factory ,modelformset_factory
import pdb


class DonationRegistrationForm(ModelForm):

	class Meta:
		model = IntendToGive
		exclude = ()


class GiveKindForm(ModelForm):

	class Meta:
		model = GiveKind
		exclude = ()

	def clean(self):
		cleaned_data = super(GiveKindForm,self).clean()

		return cleaned_data

GiveKindFormSet = inlineformset_factory(IntendToGive,GiveKind,form=GiveKindForm)		


class GiveTimeForm(ModelForm):

	class Meta:
		model = GiveTime
		exclude = ()

	def clean(self):
		cleaned_data = super(GiveTimeForm,self).clean()

		return cleaned_data

GiveTimeFormSet = inlineformset_factory(IntendToGive,GiveTime,form=GiveTimeForm)

class DonationMadeForm(ModelForm):

	class Meta:
		model = Contribution
		exclude = ()


class KindContributionForm(ModelForm):

	class Meta:
		model = KindContribution
		exclude = ()

	def clean(self):
		cleaned_data = super(KindContributionForm,self).clean()
		return cleaned_data

KindContributionFormSet = inlineformset_factory(Contribution,KindContribution,form=KindContributionForm)		


class TimeContributionForm(ModelForm):

	class Meta:
		model = TimeContribution
		exclude = ()

	def clean(self):
		cleaned_data = super(TimeContributionForm,self).clean()

		return cleaned_data

TimeContributionFormSet = inlineformset_factory(Contribution,TimeContribution,form=TimeContributionForm)