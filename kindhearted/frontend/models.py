from __future__ import unicode_literals
import random
from django.db import models
from datetime import datetime
from location.models import City, State

from django.contrib.auth.models import AbstractUser
from django.utils import timezone


CSR_APPLICABILITY = [('0','Not Selected'),('1','Annual Turn Over of 1000 Cr or more'),('2','Net Worth 500 Cr or More'),('3','Net profit 0f 5 Cr or More')]

FORM_OF_ORGANIZATION = [('1','Society under Societies Registration Act and Public Trust Act'),
('2','Company registered under Section 8/25 of Companies Act'),('3','Trust registered under Public Trust Act')]
ELIGIBILITY = [('STRONG','STRONG'),('MEDIUM','MEDIUM'),('WEAK','WEAK')]


class NGOFormOfOrganization(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s" % (self.id)

class OTHERSFormOfOrganization(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s" % (self.id)

class CorporateCSRApplicability(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s" % (self.id)

class UserProfile(AbstractUser):
    confirm_password = models.CharField(max_length=25, blank=True)
    name = models.CharField(max_length=150,blank=True)
    AbstractUser._meta.get_field('email')._unique = True
    AbstractUser._meta.get_field('email').blank = False
    AbstractUser._meta.get_field('email').null = False
    AbstractUser._meta.get_field('is_active').default = True
    mobile_number = models.IntegerField(default=0)
    website = models.CharField(max_length=25,blank=True)
    address = models.TextField(max_length=255,blank=True)
    user_type = models.CharField(max_length=10,default="")
    gender = models.CharField(max_length=10, blank=True, null=True, default="")
    form_of_organization_ngo = models.ForeignKey(NGOFormOfOrganization,blank=True,null=True)
    form_of_organization_others = models.ForeignKey(OTHERSFormOfOrganization,blank=True,null=True)
    establishment_date = models.DateField(blank=True,null=True)
    registered_act_1961 = models.BooleanField(default=True)
    registered_act_12a = models.BooleanField(default=True)
    registration_no = models.CharField(max_length=150,blank=True,null=True,default="")
    csr_applicability =  models.ForeignKey(CorporateCSRApplicability,blank=True,null=True)
    eligibility = models.CharField(choices=ELIGIBILITY,default='1',max_length=100,blank=True)
    credit_points = models.IntegerField(default=0,blank=True,null=True)
    profession = models.CharField(max_length=100,blank=True,null=True,default="NA")
    flg_profile_create = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % (self.email)


class Gang(models.Model):
    gang_name = models.CharField(max_length=255,default="")
    cause_name = models.ForeignKey('cause.Cause',blank=True)
    gang_leader = models.CharField(max_length=255, default="")
    created_on = models.DateTimeField(default=timezone.now)
    gang_pic = models.FileField(upload_to="gangs_pic", blank=True, null=True)
    description = models.TextField(blank=True)
    member = models.ManyToManyField(UserProfile,blank=True)

    def __unicode__(self):
        return "%s" %(self.gang_name)


# class GangMembership(models.Model):
#     # gang = models.ForeignKey(Gang)
#     # member = models.ForeignKey(UserProfile)
#     name = models.ForeignKey(UserProfile)

    # def __unicode__(self):
    #     return "%s -> %s" % (self.gang, self.member)


class Badge(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class BadgeEarned(models.Model):
    badge = models.ForeignKey(Badge)
    earned_by = models.ForeignKey(UserProfile)
    earned_on = models.DateTimeField(datetime.now())

    def __unicode__(self):
        return "%s -> %s" % (self.badge, self.earned_by)


class PatOnBack(models.Model):
    earned_by = models.ForeignKey(UserProfile, related_name="earned_by")
    given_by = models.ForeignKey(UserProfile, related_name="given_by")
    earned_on = models.DateTimeField(datetime.now())
    comments = models.CharField(max_length=255, default="")

    def __unicode__(self):
        return "%s -> %s" % (self.earned_by, self.given_by)


class ProfileRating(models.Model):
    user = models.ForeignKey(UserProfile, related_name="user")
    rated_by = models.ForeignKey(UserProfile, related_name="rated_by")
    rated_on = models.DateTimeField(datetime.now())
    rating = models.IntegerField(default=3)
    comments = models.TextField(blank=True, null=True, default="")

    def __unicode__(self):
        return "%s -> %s" % (self.user, self.rated_by)


class Reccommendation(models.Model):
    user = models.ForeignKey(UserProfile, related_name="recco_user")
    recco_by = models.ForeignKey(UserProfile, related_name="recco_by")
    recco_on = models.DateTimeField(datetime.now())
    recco = models.TextField(blank=True, null=True, default="")

    def __unicode__(self):
        return "%s -> %s" % (self.user, self.recco_by)


def get_random_pass():
    """
    generate random password from string
    """
    s = "A0aBbCcDd1EeF2fGg3Hh4IiJj5KkLl6MmNn7OoPp8QqR9rSsTtUuVvWwXxYyZz"
    passwd = "".join([random.choice(s) for i in range(6)])
    return passwd


class ForgotPassword(models.Model):
    """ 
    Model used to store information of Forgot password requests 
    """
    user = models.ForeignKey(UserProfile)
    hashkey = models.CharField(max_length=155)
    timestamp = models.DateField()

    def __unicode__(self):
        return self.hashkey
