from django.contrib import admin

from .models import *

admin.site.register(UserProfile)
admin.site.register(Gang)
admin.site.register(Badge)
admin.site.register(BadgeEarned)
admin.site.register(PatOnBack)
admin.site.register(ProfileRating)
admin.site.register(Reccommendation)
admin.site.register(ForgotPassword)
admin.site.register(NGOFormOfOrganization)
admin.site.register(OTHERSFormOfOrganization)
admin.site.register(CorporateCSRApplicability)