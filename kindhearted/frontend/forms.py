from django import forms
from django.contrib import auth
from django.forms import ModelForm
from frontend.models import *
from django.contrib.auth.forms import AuthenticationForm
from .backends import EmailAuthBackend
from django.contrib.auth.models import User

class SignupForm(ModelForm):
	
	class Meta:
		model = UserProfile
		exclude = ['date_joined']

	def clean(self):
		cleaned_data = super(SignupForm,self).clean()
			
		try:
			username = cleaned_data.get('username')
			email = cleaned_data.get('email')
			password = cleaned_data.get('password')
			confirm_password = cleaned_data.get('confirm_password')
			
		except:
			msg = "Enter the required fields properly"
			password = ""
			confirm_password = ""
			self._errors["password"] = self.error_class([msg])
			return self.cleaned_data

		if password is not None and confirm_password is not None:
			if not password == confirm_password:
				msg = "password and confirm password should be equal"
				# self.add_error(None,msg)
				self._errors["password"] = self.error_class([msg])
				return self.cleaned_data

		return self.cleaned_data		
		# import pdb;pdb.set_trace()
		
		#exclude = ['gender','registered_act_1961','registered_act_12a','establishment_date']
		# fields = ['username','email','password','confirm_password','name','mobile_number','website','address','user_type','form_of_organization','establishment_date']
	# def clean(self):
	# 	cleaned_data = super(SignupForm, self).clean()

	# 	try:
	# 		first_name = cleaned_data.get('first_name')
	# 	except:
	# 		first_name = ""

	# 	if not first_name:
	# 		msg = "This Field Is Required"
	# 		self._errors["first_name"] = self.error_class([msg])

	# 	try:
	# 		last_name = cleaned_data.get('last_name')
	# 	except:
	# 		last_name = ""

	# 	if not last_name:
	# 		msg = "This Field Is Required"
	# 		self._errors["last_name"] = self.error_class([msg])

	# 	try:
	# 		mobile_no = cleaned_data.get('mobile_no')
	# 	except:
	# 		mobile_no = ""

	# 	if not mobile_no:
	# 		msg = "This Field Is Required"
	# 		self._errors["mobile_no"] = self.error_class([msg])	

	# 	try:
	# 		password = cleaned_data.get("password")
	# 	except:
	# 		password = ""

	# 	if not password:
	# 		msg = "This Field is Required"
	# 		self._errors["password"] = self.error_class([msg])

	# 	# user = UserProfile.objects.filter(username=username).exists()
	# 	# if user:
	# 	# 	msg = "A User With Same Username Already Registered."
	# 	# 	self._errors["username"] = self.error_class([msg])

	# 	return self.cleaned_data


class LoginForm(forms.Form):
	"""
	Login Form
	"""
	email = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
	# model = UserProfile

	def clean(self):
		cleaned_data = super(LoginForm, self).clean()
		username = cleaned_data.get('username')
		email = cleaned_data.get('email')
		password = cleaned_data.get('password')
		
		print(username)
		print(email)
		print(password)


		if not (email or password):
			msg = "Email and Password Is Required"
			self._errors["password"] = self.error_class([msg])
			del self._errors["email"]
			return self.cleaned_data

		if not password:
		    msg = "Password Is Required"
		    self._errors["password"] = self.error_class([msg])
		    return self.cleaned_data

		if not email:
		    msg = "Email Is Required"
		    self._errors["email"] = self.error_class([msg])
		    return self.cleaned_data

		

		# if user is None:
		#     msg = "Email or Password Is Incorrect"
		#     self._errors["password"] = self.error_class([msg])
		#     return self.cleaned_data

		return self.cleaned_data    



class ForgotPasswordForm(forms.Form):
	"""
	Forgot password form
	"""
	email = forms.CharField()

	def clean(self):
		cleaned_data = super(ForgotPasswordForm, self).clean()
		email = cleaned_data.get("email")

		if not email:
			msg = "This Field is Required"
			self._errors["email"] = self.error_class([msg])
			return self.cleaned_data
		
		user = UserProfile.objects.filter(email=email)
		if user.count() == 0:
			msg = "User with this email does not exists!"
			self._errors["email"] = self.error_class([msg])
			return self.cleaned_data

		return self.cleaned_data


class ChangePasswordForm(forms.Form):
	user = forms.CharField(label="User",max_length=50,required=False)
	old_password =forms.CharField(widget=forms.PasswordInput(), label="old_password", max_length=50)
	new_password = forms.CharField(widget=forms.PasswordInput(), label="new_password", max_length=50)
	confirm_password = forms.CharField(widget=forms.PasswordInput(), label="confirm_password", max_length=50)

	def save(self,commit=True):
		instance = super(ChangePasswordForm,self).save(commit=False)
		print "instance.save()"
		print instance.save()

		return instance.save()

	def clean(self):
		cleaned_data = super(ChangePasswordForm,self).clean()
		old_password = cleaned_data.get('old_password')
		new_password = cleaned_data.get('new_password')
		confirm_password = cleaned_data.get('confirm_password')
		user = cleaned_data.get('user')

		user1 = UserProfile.objects.get(email=user)

		if not old_password and not new_password:
			print "Please Enter Old and New Password"
			msg = "Please Enter Old and New Password"
			self.errors['confirm_password'] = self.error_class([msg])
		else:
			if old_password == new_password:
				print "Old and New Password should Not Match"
				msg = "Old and New Password should Not Match"
				self.errors['confirm_password'] = self.error_class([msg])
				return self.cleaned_data

			if not user1.check_password(old_password):
				# print "Old Password does not Match with User"
				msg = "Old Password does not Match with User"
				self._errors["confirm_password"] = self.error_class([msg])
				return self.cleaned_data

			if not new_password == confirm_password:
				msg = "New Passwords do not match"
				print msg
				self.errors["confirm_password"] = self.error_class([msg])	
				return self.cleaned_data
		return self.cleaned_data    


class ChangePasswordRequestForm(forms.Form):

	"""
	Change Password Form form forgot password request
	"""

	password = forms.CharField(widget=forms.PasswordInput)
	confirm_password = forms.CharField(widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(ChangePasswordRequestForm, self).clean()

		try:
			password = cleaned_data.get('password')
		except:
			password = ''

		if not password:
			msg = "This Field Required"
			self._errors["password"] = self.error_class([msg])

		try:
			confirm_password = cleaned_data.get('confirm_password')
		except:
			confirm_password = ''

		if not confirm_password:
			msg = "Confirm Password Is Required"
			self._errors["confirm_password"] = self.error_class([msg])

		if password == confirm_password:
			pass
		else:
			msg="Both Passwords do not match"
			self._errors["confirm_password"] = self.error_class([msg])

		return self.cleaned_data
