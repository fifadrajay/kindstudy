import random
import requests
from urllib import urlencode
from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

@shared_task
def get_random_otp():
	"""
	generate random otp from string
	"""
	s = "0123456789"
	passwd = "".join([random.choice(s) for i in range(6)])
	return passwd

@shared_task
def generate_otp():
	"""
	generate otp for given user
	"""
	otp = get_random_otp()
	return otp

@shared_task
def send_otp(mobile_no, otp):
	message = "Welcome to KindHearted.com! Your OTP is %s." % otp
	msg = {"message": message}
	send_sms(mobile_no, urlencode(msg))

@shared_task
def send_otp_mobile(mobile):
	"""
	send otp msg to mobile
	"""
	otp = generate_otp()
	send_otp(mobile, otp)
	return otp

@shared_task
def send_sms(mobile_no, message):
	URL = "http://mobicomm.dove-sms.com/mobicomm//submitsms.jsp?user=Ishantr&key=6757f84f00XX&mobile=%s&%s&senderid=CLOANY&accusage=1" % (mobile_no, message)
	response = requests.get(URL)
	SMSLogs.objects.create(mobile=mobile_no, sms=message)
	return response

@shared_task
def send_changed_password(email, first_name, mobile, last_name, passwd):
	message = "Dear User. your password has been changed successfully. Your new password is %s." % passwd
	msg = {"message": message}
	# send_sms(mobile, urlencode(msg))
	send_forget_password_email(email, first_name, last_name, passwd)

# @shared_task
def send_forget_password_email(email, first_name, last_name, passwd):
	"""
	send forget password email notification
	"""
	email_subject = 'Kind Hearted: Forgot Password Request'
	email_body = """<html>
	<head>
	</head>
	<body>
	<p>
	Hello %s %s,<br><br>
	Your login password is %s.
	<br>
	<br>
	Thanks,<br>
	Kind Hearted Team<br>
	</p>
	</body>
	</html>""" % (first_name, last_name, passwd)
	msg = EmailMultiAlternatives(email_subject, '', settings.EMAIL_HOST_USER , [email])
	msg.attach_alternative(email_body, "text/html")
	msg.send()