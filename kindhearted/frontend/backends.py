from .models import UserProfile
from django.contrib.auth.backends import ModelBackend
import pdb


class EmailAuthBackend(ModelBackend):
	"""
    A custom authentication backend. Allows users to log in using their email and password
    """

	def authenticate(self,username=None, password=None):
		try:
			
			user = UserProfile.objects.get(email=username)
			if user.check_password(password):
				print("check password")
				return user
		except UserProfile.DoesNotExist:
			return None

	def get_user(self,user_id):
		try:
			user = UserProfile.objects.get(pk=user_id)
			if user.is_active:
				return user
			return None
		except UserProfile.DoesNotExist:
			return None
	