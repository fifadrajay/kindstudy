import json
import hashlib
import uuid
from uuid import uuid4
from django.shortcuts import render
from django.views.generic import TemplateView, View, FormView,CreateView,DetailView,UpdateView,ListView
from frontend.tasks import send_otp_mobile
from django.http import HttpResponse, HttpResponseRedirect
from frontend.models import *
from frontend.tasks import send_otp_mobile, send_changed_password
from frontend.forms import *
from cause.forms import *
from reciepent.forms import *
from contribution.forms import *
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.shortcuts import redirect
from django.conf import settings
from django.core.mail import send_mail
from datetime import datetime,date
from django.contrib import messages
from frontend.tokens import PasswordResetTokenGenerator
from django.template import loader
from reportlab.pdfgen import canvas
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext
from .backends import EmailAuthBackend
from django.contrib.auth import login,authenticate,logout
from django.db import transaction
from reciepent.models import *
from cause.models import *
from project.models import *
from contribution.models import *
from location.models import Location
from project.forms import *
from blog.forms import *
from django.db.models import Count,Sum
from wkhtmltopdf.views import PDFTemplateView,PDFResponse, PDFTemplateResponse
import pdb






def render_to_json_response(context, **response_kwargs):
	data = json.dumps(context)
	response_kwargs['content_type'] = 'application/json'
	return HttpResponse(data, **response_kwargs)

def base_template_name(request):
    if request.user.is_authenticated():
        base_template_name = 'base-login2.html'
    else:
        base_template_name = 'base.html'

    return base_template_name 


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self,**kwargs):
    	data = super(IndexView,self).get_context_data(**kwargs)
    	data['projects'] = Project.objects.all()[:3]
    	data['causes'] = Cause.objects.all()[:3]
    	data['causes_count'] = Cause.objects.all().count()
    	data['requests'] = IntendToReceive.objects.all()[:3]
    	data['requests_count'] = IntendToReceive.objects.all().count()
    	data['pledges'] = IntendToGive.objects.all()[:3]
    	data['contributors'] = Contribution.objects.all()[:3]
    	data['contributors_count'] = Contribution.objects.all().count()
    	data['amount_donated'] = Contribution.objects.aggregate(Sum('money'))
    	data['blogs'] = Blog.objects.all()[:3]
    	data['events'] = Event.objects.all()[:1]
    	return data



class LoginView(FormView):
	template_name = "loginmember.html"
	form_class = LoginForm
	model = UserProfile
	success_url="/dashboard/member/"

	def get_context_data(self, **kwargs):
		context = super(LoginView, self).get_context_data(**kwargs)
		return context

	def form_valid(self,form):
		email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		user = authenticate(username=email,password=password)
		fail_msg = {'auth_fail':'Invalid Username Or Password'}
		if user is not None:
			user.is_active = True
			user.save()
			login(self.request,user)
		else:
			return self.render_to_response(fail_msg)
		
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form = form))
	
	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)	

def LogoutView(request):
	logout(request)
	return HttpResponseRedirect(reverse('index'))

class RegistrationView(FormView):
	"""
	SignUpView
	"""
	template_name = "memberregistration.html"
	form_class = SignupForm
	model = UserProfile
	success_msg = "User Registered Successfully"
	success_url = "/signup/"

	def get_context_data(self, **kwargs):
		context = super(RegistrationView, self).get_context_data(**kwargs)
		context['others_form_of_organization'] = OTHERSFormOfOrganization.objects.all()
		context['ngo_form_of_organization'] = NGOFormOfOrganization.objects.all()
		context['csr_applicability'] = CorporateCSRApplicability.objects.all()
		context['msg'] = "User Registered Successfully"
		return context

	def form_valid(self,form):
		user = form.save()
		user.set_password(user.password)
		user.save()
		parameters = {'success_msg': user.username +'  Registered Successfully'}
		return self.render_to_response(parameters)


	def form_invalid(self,form): 
		return super(RegistrationView, self).form_invalid(form)

class CorporateEntityView(CreateView):
	
	model = CorporateBasic
	template_name = "member-form.html"
	form_class = CorporateBasicForm
	success_url = "/dashboard/member/"
	

	def get_context_data(self,**kwargs):
		data = super(CorporateEntityView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)
		data['user'] = user
		return data


	def form_valid(self,form):
		corporate = form.save()
		directorsformset =  CorporateDirectorFormSet(self.request.POST,self.request.FILES,prefix='director',instance=corporate)
		csrcommitteformset =  CorporateCsrCommiteeFormSet(self.request.POST,self.request.FILES,prefix='csrmember',instance=corporate)
		
		if directorsformset.is_valid():
			directorsformset.save()

		if csrcommitteformset.is_valid():
			csrcommitteformset.save()		

		user = self.request.user
		user.flg_profile_create = True
		user.save()
		parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		return self.render_to_response(parameters)
		

	def form_invalid(self,form):
		print form.errors
		return super(CorporateEntityView, self).form_invalid(form)	


class IndividualEntityView(CreateView):
	model= IndividualDetails
	template_name = "member-form.html"
	fields = '__all__'
	

	def get_context_data(self,**kwargs):
		data = super(IndividualEntityView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)
		skills = PersonalSkills.objects.all()
		data['user'] = user
		data['skills'] = skills
		return data 

	def form_valid(self,form):
		individual = form.save()
		user = self.request.user
		user.flg_profile_create = True
		user.save()
		parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		return self.render_to_response(parameters)

	def form_invalid(self,form):
		print form.errors
		return super(IndividualEntityView,self).form_invalid(form)
		

class OthersEntityView(CreateView):
	model= OthersDetails
	template_name = "member-form.html"
	fields = '__all__'
	success_url = "/entity/info/"

	def get_context_data(self,**kwargs):
		data = super(OthersEntityView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)
		skills = PersonalSkills.objects.all()
		others_form_of_organization = OTHERSFormOfOrganization.objects.all()
		data['user'] = user
		data['skills'] = skills
		data['others_form_of_organization'] = others_form_of_organization

		return data

	def form_valid(self,form):
		others = form.save()
		user = self.request.user
		user.flg_profile_create = True
		user.save()
		parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		return self.render_to_response(parameters)

	def form_invalid(self,form):
		print form.errors
		return super(OthersEntityView,self).form_invalid(form)


class ForgotPasswordView(FormView):
	"""
	docstring for forgot-password
	"""
	template_name = "forgotpassword.html"		
	form_class = ForgotPasswordForm

	def get_context_data(self, **kwargs):
		context = super(ForgotPasswordView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form):
		email = form.cleaned_data['email']
		user = UserProfile.objects.get(email=email)
		try:
			if ForgotPassword.objects.filter(user=user).exists():
				ForgotPassword.objects.filter(user=user)[0].delete()
		except ForgotPassword.DoesNotExist:
			pass
		if user:
			u = uuid.uuid4()
			salt = u.hex
			h = hashlib.sha256()
			h.update(email+salt)
			hashed_key = h.hexdigest()
			token_generator = PasswordResetTokenGenerator().make_token(user)
			activation_link = '{0}/password_setting_for_forgotpassword/{1}'.format(settings.SITE_NAME,token_generator)
			ForgotPassword.objects.create(user = user, hashkey = token_generator, timestamp = datetime.now())
			send_mail('Kind Hearted: Forgot password request',activation_link, 'vinaya@fafadiatech.com',[email], fail_silently=False);
			msg = "Check your email for further assitance for changing password"
			parameters = {'success_msg': msg }
			return self.render_to_response(parameters)
			

	def form_invalid(self, form):
		if self.request.is_ajax():
			return render_to_json_response(form.errors, status=400)
		else:
			return self.render_to_response(self.get_context_data(form=form))


class ForgotPasswordLinkView(FormView):

	template_name = "forgot-password-link.html"
	form_class = ChangePasswordRequestForm
	success_url = '/'

	def get_context_data(self, **kwargs):
		context = super(ForgotPasswordLinkView, self).get_context_data(**kwargs)
		if ForgotPassword.objects.filter(hashkey=self.kwargs['token']).exists():
			context['obj'] =  ForgotPassword.objects.get(hashkey=self.kwargs['token'])
		return context

	def get(self, request, token):
		try:
			obj = ForgotPassword.objects.get(hashkey=token)
			token_valid = PasswordResetTokenGenerator().check_token(obj,token)
			if token_valid:
				if obj:
					template = loader.get_template("forgot-password-link.html")
		    		return render_to_response('forgot-password-link.html', RequestContext(request))
		except ForgotPassword.DoesNotExist:
			pass
		template = loader.get_template("link-expired.html")
		return render_to_response('link-expired.html', RequestContext(request))

	def form_valid(self, form):
		try:
			obj = ForgotPassword.objects.get(hashkey=self.kwargs['token'])
			if obj:
				password = form.cleaned_data['password']
				user = UserProfile.objects.get(pk = obj.user_id)
				if user:
					user.set_password(password)
					user.save()
					messages.success(self.request, "Password changed successfully....!")
					ForgotPassword.objects.filter(hashkey = self.kwargs['token']).delete()
				return self.render_to_response(self.get_context_data(form=form))
		except ForgotPassword.DoesNotExist:
			messages.error(self.request, "Error ....!")
			return render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))


class CommunityView(TemplateView):
	template_name = "legal.html"

class CommunityCausesView(ListView):
	template_name = "causeslist.html"
	model = Cause


class CommunityNewCauseView(TemplateView):
	template_name = "newcause.html"

class BlogListView(TemplateView):
	template_name = "bloglist.html"
	
	def get_context_data(self,**kwargs):
		data = super(BlogListView,self).get_context_data(**kwargs)
		data['all_blogs'] = Blog.objects.all()
		return data

class MemberBlogListView(TemplateView):
	template_name = "memberbloglist.html"
	
	def get_context_data(self,**kwargs):
		data = super(MemberBlogListView,self).get_context_data(**kwargs)
		data['all_blogs'] = Blog.objects.all()
		return data

class BlogDetailView(DetailView):
	template_name = "blogdetail.html"
	model = Blog
	def get_context_data(self,**kwargs):
		data = super(BlogDetailView,self).get_context_data(**kwargs)
		data['all_blogs'] = Blog.objects.all()
		return data

class EventDetailView(DetailView):
	template_name = "eventdetail.html"
	model = Event


class HowItWorksView(TemplateView):
	template_name = "howitworks.html"	

class ContactUsView(TemplateView):
	template_name = "contactus.html"

class AdvisorsView(TemplateView):
	template_name = "advisors.html"		

class FrequentQuestionsView(TemplateView):
	template_name = "frequentquestions.html"	

class DonateView(TemplateView):
	template_name = "donate.html"	

class AllCommunityView(TemplateView):
	template_name = "all-community.html"

	def get_context_data(self,**kwargs):
		data = super(AllCommunityView,self).get_context_data(**kwargs)
		data['contributors'] = Contribution.objects.all()[:4]
		data['receipients'] = IntendToReceive.objects.all()[:3]
		data['projects'] = Project.objects.all()[:3]
		return data

class RecipientsView(ListView):
	template_name = "recipients.html"
	model = IntendToReceive

class PhotosView(TemplateView):

	template_name = "gallery-photos.html"
	form_class= GalleryPhotoForm
	success_url = "/photos/"

	def get_context_data(self,**kwargs):
		data = super(PhotosView,self).get_context_data(**kwargs)
		all_photos =  PhotoGallery.objects.all()
		event_photos = Event.objects.all()
		photo = PhotoGallery.objects.all()
		data['photos'] = photo
		data['all_photos'] = all_photos
		data['event_photos'] = event_photos

		return data


class VideosView(TemplateView):
	template_name = "gallery-videos.html"
	form_class= GalleryVideoForm
	success_url = "/videos/"

	def get_context_data(self,**kwargs):
		data = super(VideosView,self).get_context_data(**kwargs)
		all_videos =  VideoGallery.objects.all()
		causes_videos = Cause.objects.all()
		event_videos = Event.objects.all()
		data['all_videos'] = all_videos
		data['causes_videos'] = causes_videos
		data['event_videos'] = event_videos

		return data

class MemberPhotosView(FormView):
	template_name = "memberphotos.html"

	form_class= MemberPhotoForm
	success_url = "/member/photos/"

	def get_context_data(self,**kwargs):
		data = super(MemberPhotosView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)

		user_photos = PhotoGallery.objects.filter(created_by=user)
		all_photos =  PhotoGallery.objects.all()
		event_photos = Event.objects.all()
		
		data['user'] = user
		data['user_photos'] = user_photos
		data['all_photos'] = all_photos
		data['event_photos'] = event_photos

		return data

	def form_valid(self,form):
		photo = form.save()
		return super(MemberPhotosView, self).form_valid(form)

	def form_invalid(self,form):
		return super(MemberPhotosView,self).form_invalid(form)	 	

class MemberVideosView(FormView):
	template_name = "membervideos.html"

	form_class= MemberVideoForm
	success_url = "/member/videos/"

	def get_context_data(self,**kwargs):
		data = super(MemberVideosView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)

		user_videos = VideoGallery.objects.filter(created_by=user)
		all_videos =  VideoGallery.objects.all()
		event_videos = Event.objects.all()
		
		data['user'] = user
		data['user_videos'] = user_videos
		data['all_videos'] = all_videos
		data['event_videos'] = event_videos

		return data

	def form_valid(self,form):
		video = form.save()
		return super(MemberVideosView, self).form_valid(form)

	def form_invalid(self,form):
		return super(MemberVideosView,self).form_invalid(form)

class CauseView(DetailView):
	template_name = "cause.html"
	model = Cause

class CreateGangView(TemplateView):
	template_name = "newgang.html"	

class GangListView(TemplateView):
	template_name = "ganglist.html"	

class EditProfileView(TemplateView):
	template_name = "editprofile.html"	

class ChangePasswordView(FormView):
	template_name = "changepassword.html"
	form_class = ChangePasswordForm	

	def form_valid(self,form):
		confirm_password = form.cleaned_data['confirm_password']
		current_user = UserProfile.objects.get(email=self.request.user)
		current_user.set_password(confirm_password)
		current_user.save()
		return HttpResponseRedirect(reverse('login'))

	def form_invalid(self,form):
		return super(ChangePasswordView, self).form_invalid(form)		

class DonateRegistrationView(FormView):
	template_name = "donate-reg.html"
	form_class = DonationRegistrationForm
	model = IntendToGive
	success_url = '/mydonations/'

	def get_context_data(self,**kwargs):
		data = super(DonateRegistrationView,self).get_context_data(**kwargs)
		data['user'] = self.request.user
		data['communities'] = Community.objects.all()
		data['locations'] = Location.objects.all()
		data['causes'] = Cause.objects.all()
		data['subcauses'] = SubCause.objects.all()
		data['money_freq'] = MoneyFrequency.objects.all()
		data['kind_freq'] = KindFrequency.objects.all()
		data['kind_type'] = KindType.objects.all()
		data['kind_subtype'] = KindSubType.objects.all()
		data['time_skilltype'] = TimeSkillType.objects.all()
		data['time_subskill'] = TimeSkillSubType.objects.all()
		data['time_freq'] = TimeFrequency.objects.all()
		return data

	def form_valid(self,form,*args,**kwargs):
		donation = form.save()
		kind = GiveKindFormSet(self.request.POST,self.request.FILES,prefix='kind',instance=donation)
		time = GiveTimeFormSet(self.request.POST,prefix='time',instance=donation)

		if kind.is_valid():
			kind.save()

		if time.is_valid():
			time.save()

		parameters = {'success_msg': 'Your Donation Pledge is Recorded Success'}
		return self.render_to_response(parameters)

	def form_invalid(self,form,*args,**kwargs):
		return super(DonateRegistrationView,self).form_valid(form)	
				
class RequestView(FormView):
	template_name = "makerequest.html"
	form_class = RequestForm
	success_url = '/myrequests/'
	model = IntendToReceive

	def get_context_data(self,**kwargs):
		data = super(RequestView,self).get_context_data(**kwargs)
		data['user'] = self.request.user
		data['communities'] = Community.objects.all()
		data['locations'] = Location.objects.all()
		data['causes'] = Cause.objects.all()
		data['subcauses'] = SubCause.objects.all()
		data['money_freq'] = MoneyFrequency.objects.all()
		data['kind_freq'] = KindFrequency.objects.all()
		data['kind_type'] = KindType.objects.all()
		data['kind_subtype'] = KindSubType.objects.all()
		data['time_skilltype'] = TimeSkillType.objects.all()
		data['time_subskill'] = TimeSkillSubType.objects.all()
		data['time_freq'] = TimeFrequency.objects.all()
		return data

	def form_valid(self,form,*args,**kwargs):
		receive = form.save()
		kind = ReceiveKindFormSet(self.request.POST,self.request.FILES,prefix='kind',instance=receive)
		time = ReceiveTimeFormSet(self.request.POST,prefix='time',instance=receive)

		if kind.is_valid():
			kind.save()
			

		if time.is_valid():
			time.save()	

		return super(RequestView,self).form_valid(form)

	def form_invalid(self,form,*args,**kwargs):
		print form.errors
		return super(RequestView,self).form_invalid(form)	



class HistoryView(TemplateView):
	template_name = "history.html"	

class MyRequestView(TemplateView):
	template_name = "myrequests.html"

	def get_context_data(self,**kwargs):
		data = super(MyRequestView,self).get_context_data(**kwargs)
		user = self.request.user
		data['requests'] = IntendToReceive.objects.filter(user=user)
		return data

class MyDonateView(TemplateView):
	template_name = "mydonations.html"

	def get_context_data(self,**kwargs):
		user = self.request.user
		data = super(MyDonateView,self).get_context_data(**kwargs)
		data['contributions'] = Contribution.objects.filter(contributed_by=user)
		return data

class RequestDetailView(TemplateView):
	template_name = "requestdetail1.html"	

class ContributionDetailView(TemplateView):
	template_name = "donationdetail.html"	

class AdminDashboardView(TemplateView):
	template_name = "admindashboard.html"	

class AdminUserProfileView(TemplateView):
	template_name = "adminuserprofile.html"	

class AdminPhotosView(TemplateView):
	template_name = "adminphotos.html"		

class AdminVideosView(TemplateView):
	template_name = "adminvideos.html"	

class AdminBlogsView(TemplateView):
	template_name = "adminblogs.html"		

class AdminUserProfileDetailView(TemplateView):
	template_name = "adminuserdetail.html"		

class MemberDashboardView(TemplateView):
	template_name = "memberdashboard.html"

	def get_context_data(self,**kwargs):
		context = super(MemberDashboardView,self).get_context_data(**kwargs)
		context['username'] = self.request.user.username
		context['requests'] = IntendToReceive.objects.all()[:3]
		context['contributors'] = IntendToGive.objects.all()[:3]
		context['projects'] = Project.objects.all()[:3]
		context['user_id'] = self.request.user.id
		return context 


class MemberPostLoginView(TemplateView):
	template_name = "memberpostlogin.html"	

class SendOTPView(View):
	"""
	send otp to mobile
	"""
	def get(self, request, *args, **kwargs):
		mobile =  request.GET.get('mob_num')
		msg = "One Time Password (OTP) has been sent to your Mobile"
		return render_to_json_response({'success':msg}, status=400)


class MemberReturnCommunityView(TemplateView):
	template_name = "return-community.html"		

class MemberGangsView(TemplateView):
	template_name = "gangs.html"		

	def get_context_data(self,**kwargs):
		data = super(MemberGangsView,self).get_context_data(**kwargs)
		data['user'] = self.request.user
		data['all_gangs'] = Gang.objects.all().annotate(countmember=Count('member'))
		data['own_gangs'] = Gang.objects.annotate(countmember=Count('member')).filter(member=self.request.user)
		return data

class GangJoiningView(TemplateView):
	template_name = "gangjoininginvite.html"	

class AdminUsersView(TemplateView):
	template_name = "adminusers.html"	

class AdminUserDetailView(TemplateView):
	template_name = "adminmemberdetails.html"	

class MemberBlogsView(DetailView):
	template_name = "memberblogs.html"
	model = Blog

	def get_context_data(self,**kwargs):
		data = super(MemberBlogsView,self).get_context_data(**kwargs)
		return data

class MyBlogsView(DetailView):
	template_name = "myblogs.html"
	model = Blog

	def get_context_data(self,**kwargs):
		data = super(MyBlogsView,self).get_context_data(**kwargs)
		return data


class MemberCreateBlog(FormView):
	template_name = "member-create-blog.html"
	form_class = MemberCreateBlogForm
	model = Blog
	success_url = '/member/blogs/'

	def get_context_data(self,**kwargs):
		data = super(MemberCreateBlog,self).get_context_data(*kwargs)
		data['causes'] = Cause.objects.all()
		return data

	def form_valid(self,form):
		blog = form.save()
		return super(MemberCreateBlog,self).form_valid(form)

	def form_invalid(self,form):
		return super(MemberCreateBlog,self).form_invalid(form)

class MemberOwnBlogsView(TemplateView):
	template_name = "memberownblogs.html"

	def get_context_data(self,**kwargs):
		data = super(MemberOwnBlogsView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		own_blogs = Blog.objects.filter(user_id=user_id)
		data['own_blogs'] = own_blogs
		return data

class AdminCauseView(TemplateView):
	template_name = "admincauses.html"


class AdminCreateProfileView(TemplateView):
	template_name = "admin-create-profile.html"

class EntityFormView(TemplateView):
	template_name = "member-form.html"

	def get_context_data(self,**kwargs):
		data = super(EntityFormView,self).get_context_data(**kwargs)
		data['purposes'] = OrganizationPurpose.objects.all()
		data['ngo_form_of_organisation'] = NGOFormOfOrganization.objects.all()
		data['others_form_of_organization'] = OTHERSFormOfOrganization.objects.all()
		data['locations'] = Location.objects.all()
		data['csr_applicability'] = CorporateCSRApplicability.objects.all()
		skills = PersonalSkills.objects.all()
		data['skills'] = skills
		return data

class NGOEntityView(CreateView):
	template_name = "member-form.html"
	model = NgoBasic
	fields = '__all__'
	

	def get_context_data(self,**kwargs):
		data = super(NGOEntityView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		user = UserProfile.objects.get(id=user_id)
		data['user'] = user
		data['ngo_form_of_organisation'] = NGOFormOfOrganization.objects.all()
		data['locations'] = Location.objects.all()
		skills = PersonalSkills.objects.all()
		data['skills'] = skills
		return data


	def form_valid(self,form,*args,**kwargs):
		user = self.request.user
		form2 = NgoBasicForm(self.request.POST,self.request.FILES,prefix='ngobasic')
		form3 = OrganizationDetailsForm(self.request.POST,self.request.FILES,prefix='organizationdetail')
		form4 = FinancialDetailsFormset(self.request.POST,prefix='findetail',instance=user)
		form5 = AddPastProjectFormset(self.request.POST,prefix='project',instance=user)

		form.save()
		form3.save()
		financial_date = date(2017, 7, 7)

		if form4.is_valid():
			form4_list = form4.save()
			for fin in form4_list:
				fin.financialdate = financial_date
				fin.save()

		if form5.is_valid():
			form5.save()

		user = self.request.user
		user.flg_profile_create = True
		user.save()
		parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		return self.render_to_response(parameters)
		

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form = form))

class ContributorDetailView(DetailView):
	model = IntendToGive
	template_name = "contributor-detail.html"

	def get_context_data(self,**kwargs):
		data = super(ContributorDetailView,self).get_context_data(**kwargs)
		user_id = self.request.user.id
		data['user'] = UserProfile.objects.get(id=user_id)
		data['myrequests'] = IntendToReceive.objects.filter(user=user_id)
		data['pk'] = self.kwargs['pk']
		return data
	
class RequestorDetailView(DetailView):
	model = IntendToReceive
	template_name = "requestor-detail.html"

	def get_context_data(self,**kwargs):
		data = super(RequestorDetailView,self).get_context_data(**kwargs)
		data['money_freq'] = MoneyFrequency.objects.all()
		data['kind_freq'] = KindFrequency.objects.all()
		data['kind_type'] = KindType.objects.all()
		data['kind_subtype'] = KindSubType.objects.all()
		data['time_skilltype'] = TimeSkillType.objects.all()
		data['time_subskill'] = TimeSkillSubType.objects.all()
		data['time_freq'] = TimeFrequency.objects.all()
		data['pk'] = self.kwargs['pk']
		data['base_template_name'] = base_template_name(self.request)
		return data

class DonationMade(FormView):
	model = Contribution
	form_class = DonationMadeForm
	template_name = 'requestor-detail.html'

	def get_context_data(self,**kwargs):
		data = super(DonationMade,self).get_context_data(**kwargs)
		data['user'] = self.request.user.id
		return data

	def get_success_url(self,request_id):
		request_id = request_id
		return reverse('requestordetail',kwargs = {'pk' : request_id})

	def form_valid(self,form):
		donation = form.save()
		kind = KindContributionFormSet(self.request.POST,self.request.FILES,prefix='kind',instance=donation)
		time = TimeContributionFormSet(self.request.POST,prefix='time',instance=donation)
		request_id = form.cleaned_data['request_id']
		if kind.is_valid():
			kind.save()

		if time.is_valid():
			time.save()
		return HttpResponseRedirect(self.get_success_url(request_id))

	def form_invalid(self,form):
		return super(DonationMade,self).form_invalid(form)	

class DonationDetailView(DetailView):
	template_name = "donation-detail.html"
	model = Contribution	

class DonationReceivedView(DetailView):
	template_name = "donation-received.html"
	model = IntendToReceive	

class CreateProjectView(FormView):

	model= Project
	template_name = "create-project.html"
	form_class = CreateProjectForm
	success_url = "/start/"

	def get_context_data(self,**kwargs):
		data = super(CreateProjectView,self).get_context_data(**kwargs)
		user_id = self.request.user.id

		data['user'] = UserProfile.objects.get(id=user_id)
		data['cause_categorys'] = CauseCategory.objects.all()
		data['causes'] = Cause.objects.all()
		data['subcauses'] = SubCause.objects.all()
		data['money_frequency'] = MoneyFrequency.objects.all()
		data['kind_type'] = KindType.objects.all()
		data['kind_subtype'] = KindSubType.objects.all()
		data['kind_frequency'] = KindFrequency.objects.all()
		data['time_skilltype'] = TimeSkillType.objects.all() 
		data['time_subskilltype'] = TimeSkillSubType.objects.all()
		data['time_frequency'] = TimeFrequency.objects.all()

		return data 

	def form_valid(self,form):
		project = form.save()
		kind = ProjectKindFormSet(self.request.POST,self.request.FILES,prefix='kind',instance=project)
		time = ProjectTimeFormSet(self.request.POST,prefix='time',instance=project)
		if kind.is_valid():
			kind.save()

		if time.is_valid():
			time.save()
		parameters = {'success_msg': project.title+' Registered Successfully'}
		return self.render_to_response(parameters)

	def form_invalid(self,form):
		parameters = {'fail_msg': 'Entered Project Details is Invalid', 'form': form}
		return self.render_to_response(parameters)		

class ProjectDetailView(FormView):
	model = ProjectFaq
	form_class = FrequentQuestionsForm
	template_name = "projectdetail.html"

	def get_context_data(self,**kwargs):
		data = super(ProjectDetailView,self).get_context_data(**kwargs)
		data['pk'] = self.kwargs['pk']
		data['project'] = Project.objects.get(id = data['pk'])
		data['project_contribution'] = Contribution.objects.filter(project_id=1)
		data['faqs'] = ProjectFaq.objects.filter(project_id = data['project'])
		data['updates'] = ProjectUpdates.objects.filter(project_id = data['project'])
		data['base_template_name'] = base_template_name(self.request)
		return data

	def get_success_url(self,**kwargs):
		project_id = self.kwargs['pk']
		return reverse('projectdetail',kwargs = {'pk' : project_id} )

	def form_valid(self,form):
		question = form.save()
		return HttpResponseRedirect(self.get_success_url())


	def form_invalid(self,form):
		return super(ProjectDetailView,self).form_invalid(form)

	

class AllProjectView(TemplateView):
	template_name = "projects-all.html"	

class ProjectsView(TemplateView):
	template_name = "projects.html"		

class ReadProfileView(DetailView):
	template_name = "view-profile.html"
	model = UserProfile

class ReadProfileNGOView(TemplateView):
	template_name = "viewprofile-ngo.html"

class ReadProfileIndividualView(TemplateView):
	template_name = "viewprofile-individual.html"

class ReadProfileOthersView(TemplateView):
	template_name = "viewprofile-others.html"		

class EditNGOProfileView(UpdateView):		
	template_name = "edit-profile.html"
	model = UserProfile
	form_class = UserProfileNGOUpdateForm
	
	

	def get_context_data(self,**kwargs):
		data = super(EditNGOProfileView,self).get_context_data(**kwargs)
		data['pk'] = self.kwargs['pk']
		data['ngo_form_of_organisation'] = NGOFormOfOrganization.objects.all()
		data['locations'] = Location.objects.all()
		data['purposes'] = OrganizationPurpose.objects.all()
		return data

	def get_success_url(self,**kwargs):
		user_id = self.kwargs['pk']
		return reverse('viewprofile',kwargs = {'pk' : user_id})			



	def form_valid(self,form):
		user = form.save()

		basic_id = self.request.POST['ngobasic-id']
		organizationdetail_id = self.request.POST['organizationdetail-id']
		project_id = self.request.POST['project-0-id']

		basic = NgoBasic.objects.get(id=basic_id)
		organizationdetail = OrganizationDetails.objects.get(id=organizationdetail_id)
		project = AddPastProject.objects.get(id=project_id)

		form2 = NgoBasicForm(self.request.POST,self.request.FILES,prefix='ngobasic',instance=basic)
		form3 = OrganizationDetailsForm(self.request.POST,self.request.FILES,prefix='organizationdetail',instance=organizationdetail)
		form4 = FinancialDetailsFormset(self.request.POST,prefix='findetail',instance=user)
		form5 = AddPastProjectFormset(self.request.POST,prefix='project',instance=user)

		form2.save()
		form3.save()

		if form5.is_valid():
			form5.save()

		if form4.is_valid():
			form4.save()

		# parameters = { 'success_msg' :'Profile Updated Successfully Which is Subjected to Verification by KH'}
		# return self.render_to_response(parameters)
		# return super(EditNGOProfileView,self).form_valid(form)
		return HttpResponseRedirect(self.get_success_url())
		

	def form_invalid(self,form):
		return super(EditNGOProfileView,self).form_invalid(form)		


class EditCorporateProfileView(UpdateView):
	template_name = "edit-profile.html"
	model = UserProfile
	form_class = UserProfileCorporateUpdateForm
	

	def get_context_data(self,**kwargs):
		data = super(EditCorporateProfileView,self).get_context_data(**kwargs)
		data['csr_applicability'] = CorporateCSRApplicability.objects.all()
		return data

	def get_success_url(self,**kwargs):
		user_id = self.kwargs['pk']
		return reverse('viewprofile',kwargs = {'pk' : user_id})

	def form_valid(self,form):
		user = form.save()
		corporate_id = self.request.POST['corporatebasic-id']
		corporate_instance = CorporateBasic.objects.get(id=corporate_id)
		corporatebasic = CorporateBasicForm(self.request.POST,self.request.FILES,prefix='corporatebasic',instance=corporate_instance)

		if corporatebasic.is_valid():
			corporate = corporatebasic.save()

			directorsformset =  CorporateDirectorFormSet(self.request.POST,self.request.FILES,prefix='director',instance=corporate)
			csrcommitteformset =  CorporateCsrCommiteeFormSet(self.request.POST,self.request.FILES,prefix='csrmember',instance=corporate)
			
			if directorsformset.is_valid():
				directorsformset.save()

			if csrcommitteformset.is_valid():
				csrcommitteformset.save()		

		# parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		# return self.render_to_response(parameters)
		return HttpResponseRedirect(self.get_success_url())
		

	def form_invalid(self,form):
		return super(EditCorporateProfileView,self).form_invalid(form)

class EditIndividualProfileView(UpdateView):
	template_name = "edit-profile.html"
	model = UserProfile
	form_class = UserProfileIndividualUpdateForm
	

	def get_context_data(self,**kwargs):
		data = super(EditIndividualProfileView,self).get_context_data(**kwargs)
		skills = PersonalSkills.objects.all()
		data['skills'] = skills
		return data 

	def get_success_url(self,**kwargs):
		user_id = self.kwargs['pk']
		return reverse('viewprofile',kwargs = {'pk' : user_id})	

	def form_valid(self,form):
		user = form.save()
		individual_id = self.request.POST['individual-id']
		individual_instance = IndividualDetails.objects.get(id=individual_id)
		individual = IndividualDetailsForm(self.request.POST,self.request.FILES,prefix='individual',instance=individual_instance)

		if individual.is_valid():  
			individual = individual.save()

		# parameters = { 'success_msg' :'Profile Created Successfully Which is Subjected to Verification by KH'}
		# return self.render_to_response(parameters)
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return super(EditIndividualProfileView, self).form_invalid(form)

class EditOtherProfileView(UpdateView):
	template_name = "edit-profile.html"
	model = UserProfile
	form_class = UserProfileOtherUpdateForm
	

	def get_context_data(self,**kwargs):
		data = super(EditOtherProfileView,self).get_context_data(**kwargs)
		data['form_of_organization_others'] = OTHERSFormOfOrganization.objects.all()
		return data

	def get_success_url(self,**kwargs):
		user_id = self.kwargs['pk']
		return reverse('viewprofile',kwargs = {'pk' : user_id})		

	def form_valid(self,form):
		user = form.save()
		others_id = self.request.POST['others-id']
		others_instance = OthersDetails.objects.get(id=others_id)
		others = OthersDetailsForm(self.request.POST,self.request.FILES,prefix='others',instance=others_instance)

		if others.is_valid():  
			others = others.save()

		
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return super(EditOtherProfileView, self).form_invalid(form)


class AdminBlogDetailView(TemplateView):
	template_name = "admin-blogdetail.html"	

class RequestGenerateView(TemplateView):
	template_name = "generated-request.html"

class SearchResultView(TemplateView):
	template_name = "search-result.html"

	def get_context_data(self,**kwargs):
		context = super(SearchResultView, self).get_context_data(**kwargs)
		query = self.kwargs['query']
		context['query'] = query
		return context

class PrintCertificateView(DetailView):
	template_name = "viewprint-certificate.html"
	model = Contribution

class ExportPdfView(View):
	template_name = "viewprint-certificate.html"
	def get(self,request,*args,**kwargs):
		return pdfview(request)	

class MyPDF(PDFTemplateView):
	
	# template_name = 'viewprint-certificate.html'
	filename = 'my_pdf.pdf'
	template_name =  settings.BASE_DIR+'/templates/viewprint-certificate.html'
	# context = {'title':'Donation Certificate'}
	

	# def get(self,request):
	# 	response = PDFTemplateResponse(request=request,template=self.template_name, filename="donation-Certificate.pdf",show_content_in_browser=False,context=self.context,cmd_options={'margin-top': 10})
	# 	return response

def pdfview(request):
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename="demo.pdf"'
	p = canvas.Canvas(response)
	p.drawString(100, 100, "Hello world.")
	p.showPage()
	p.save()
	return response


class AdminActiveRequestView(TemplateView):
	template_name = "adm-activerequests.html"

class AdminActiveDonationView(TemplateView):
	template_name = "adm-activedonations.html"	

class AdminProjectDetailView(TemplateView):
	template_name = "adm-projectdetail.html"

class AdminCreateProjectView(TemplateView):
	template_name = "adm-createproject.html"		

class AdminGangActivityView(TemplateView):
	template_name = "adm-gangactivity-page.html"		

class AdminNewBlog(TemplateView):
	template_name = "adm-newblog.html"	

