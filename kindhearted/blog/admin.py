from django.contrib import admin

from .models import *

admin.site.register(Tag)
admin.site.register(Entry)
admin.site.register(Blog)
admin.site.register(BlogComment)
