from django.db import models
from django.core.urlresolvers import reverse
from frontend.models import UserProfile
from cause.models import * 
from django.utils import timezone


class Tag(models.Model):
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.slug


class EntryQuerySet(models.QuerySet):
    def published(self):
        return self.filter(publish=True)


class Entry(models.Model):
    title = models.CharField(max_length=200)
    writer = models.CharField(max_length=200, blank=True, null=True)
    blog_cover_image = models.FileField(upload_to="blog_cover_images", blank=True, null=True)
    body = models.TextField()
    slug = models.SlugField(max_length=200, unique=True)
    publish = models.BooleanField(default=True)
    created = models.DateField(blank=True, null=True)
    modified = models.DateField(blank=True, null=True)
    tags = models.ManyToManyField(Tag)
    report_file = models.FileField(upload_to="download_files", null=True, blank=True)
    objects = EntryQuerySet.as_manager()

    def __str__(self):
        return self.title.encode('ascii', errors='replace')

    def get_absolute_url(self):
        return reverse("blog-detail", kwargs={"slug": self.slug})

    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-created"]


class Blog(models.Model):
    user_id = models.ForeignKey(UserProfile)
    blog_title = models.CharField(max_length=255,blank=False,default="")
    blog_content = models.TextField(blank=False,default="")
    blog_image = models.ImageField(upload_to="blog_pics",blank=True,null=True)
    created_on = models.DateField(default=timezone.now,blank=True,null=True)
    cause = models.ForeignKey(Cause,blank=True,null=True)

    def __str__(self):
        return "%s" %(self.blog_title)

    def __unicode__(self):
        return "%s" %(self.blog_title)    


class BlogComment(models.Model):
    blog_name = models.ForeignKey(Blog)
    commented_by = models.ForeignKey(UserProfile)
    commented_on = models.DateField(default=timezone.now)
    blog_comment = models.TextField()

    def __unicode__(self):
        return "%s" %(self.blog_name)
