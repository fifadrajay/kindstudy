from django.contrib import admin

from .models import *

admin.site.register(Pincode)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Location)