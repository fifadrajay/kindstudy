from __future__ import unicode_literals
from django.db import models
# Create your models here.

class Pincode(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=255)
    pincode = models.ForeignKey(Pincode, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = 'Cities'

    def __unicode__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=255)
    city = models.ForeignKey(City, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Location(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s" %(self.name)