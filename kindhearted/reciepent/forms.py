from reciepent.models import *
from frontend.models import UserProfile
from django.forms import ModelForm ,inlineformset_factory ,modelformset_factory
import pdb

# --------------------
class CorporateBasicForm(ModelForm):

	class Meta:
		model = CorporateBasic
		exclude = ()

class UserProfileCorporateUpdateForm(ModelForm):

	class Meta:
		model = UserProfile
		fields = ['name','website','address','registration_no','csr_applicability']



class CorporateOfficeBearersForm(ModelForm):

	class Meta:
		model = CorporateOfficeBearers
		exclude = ()

	def clean(self):
		cleaned_data = super(CorporateOfficeBearersForm,self).clean()
		return cleaned_data

CorporateDirectorFormSet = inlineformset_factory(CorporateBasic,CorporateOfficeBearers,form=CorporateOfficeBearersForm)


class CorporateCSRCommiteeForm(ModelForm):

	class Meta:
		model = CorporateCSRCommitee
		exclude = ()

	def clean(self):
		cleaned_data = super(CorporateCSRCommiteeForm,self).clean()
		return cleaned_data

CorporateCsrCommiteeFormSet = inlineformset_factory(CorporateBasic,CorporateCSRCommitee,form=CorporateCSRCommiteeForm)		

# --------------------------------

class UserProfileBasicForm(ModelForm):

	class Meta:
		model = UserProfile
		fields = ['name','establishment_date']

	def clean(self):
		cleaned_data = super(UserProfileBasicForm,self).clean()	
		return cleaned_data
# --------------------------------

class UserProfileNGOUpdateForm(ModelForm):

	class Meta:
		model = UserProfile
		fields = ['username','name','mobile_number','website','address','form_of_organization_ngo','establishment_date','registered_act_1961','registered_act_12a']

	def clean(self):
		cleaned_data = super(UserProfileNGOUpdateForm,self).clean()

		establishment_date = cleaned_data.get('establishment_date')
		print establishment_date
		return cleaned_data	

class NgoBasicForm(ModelForm):

	class Meta:
		model = NgoBasic
		exclude = ()

	

class OrganizationDetailsForm(ModelForm):

	class Meta:
		model = OrganizationDetails
		exclude = ()

	def clean(self):
		cleaned_data = super(OrganizationDetailsForm,self).clean()	
		return cleaned_data	


class NgoFinancialDetailsForm(ModelForm):

	class Meta:
		model = NgoFinancialDetails
		exclude = ()

	def clean(self):
		cleaned_data = super(NgoFinancialDetailsForm,self).clean()

FinancialDetailsFormset = inlineformset_factory(UserProfile,NgoFinancialDetails,form=NgoFinancialDetailsForm)

class NgoAddPastProjectForm(ModelForm):

	class Meta:
		model = AddPastProject
		exclude = ()

	def clean(self):
		cleaned_data = super(NgoAddPastProjectForm,self).clean()

AddPastProjectFormset = inlineformset_factory(UserProfile,AddPastProject,form=NgoAddPastProjectForm)	

# --------------------------------

# --------------------------------

class UserProfileIndividualUpdateForm(ModelForm):

	class Meta:
		model = UserProfile
		fields = ['name','mobile_number','address','website']


class IndividualDetailsForm(ModelForm):

	class Meta:
		model = IndividualDetails
		exclude = ()

# --------------------------------
class UserProfileOtherUpdateForm(ModelForm):

	class Meta:
		model = UserProfile
		fields = ['name','address','website','form_of_organization_others']	

class OthersDetailsForm(ModelForm):

	class Meta:
		model = OthersDetails
		exclude = ()
# --------------------------------


class RequestForm(ModelForm):

	class Meta:
		model = IntendToReceive
		exclude = ()


class ReceiveKindForm(ModelForm):

	class Meta:
		model = ReceiveKind
		exclude = ()

	def clean(self):
		cleaned_data = super(ReceiveKindForm,self).clean()

		return cleaned_data

ReceiveKindFormSet = inlineformset_factory(IntendToReceive,ReceiveKind,form=ReceiveKindForm)		


class ReceiveTimeForm(ModelForm):

	class Meta:
		model = ReceiveTime
		exclude = ()

	def clean(self):
		cleaned_data = super(ReceiveTimeForm,self).clean()

		return cleaned_data

ReceiveTimeFormSet = inlineformset_factory(IntendToReceive,ReceiveTime,form=ReceiveTimeForm)