from django.contrib import admin

from .models import *

admin.site.register(CorporateBasic)
admin.site.register(CorporateOfficeBearers)
admin.site.register(CorporateCSRCommitee)
admin.site.register(IndividualDetails)
admin.site.register(PersonalSkills)
admin.site.register(OthersDetails)
admin.site.register(OthersFormOfOrganisation)
admin.site.register(OrganizationDetails)
admin.site.register(NgoFinancialDetails)
admin.site.register(NgoBasic)
admin.site.register(NgoFormOfOrganisation)
admin.site.register(IntendToReceive)
admin.site.register(ReceiveKind)
admin.site.register(ReceiveTime)
admin.site.register(AddPastProject)
admin.site.register(ReturnToCommunityObligations)
admin.site.register(ReturnToCommunityCompleted)
admin.site.register(OrganizationPurpose)


# Register your models here.
