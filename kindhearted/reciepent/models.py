from __future__ import unicode_literals

from django.db import models

from cause.models import Cause,SubCause,Community
from frontend.models import UserProfile
from datetime import date
from frontend.models import UserProfile
from project.models import Project
from contribution.models import MoneyFrequency,KindType,KindSubType,KindFrequency,TimeSkillType,TimeSkillSubType,TimeFrequency,Contribution
from django.utils import timezone

DONOR_BENEFIT_SECTION_CHOICE = [('1','80G'),('2','80GGA')]
RATE_OF_DEDUCTION_CHOICE = [('1','50%'),('2','100%')]
RELIGIOUS_REASON_CHOICE = [('1','100% Charitable Purpose'),
('2','100% Religious Purpose'),('3','Partly Charitable and Partly Religious (of National Importance)'),
('4','Partly Charitable and Partly Religious (Others)')]
CSR_READY_CHOICE = [(1,'YES'),(2,'NO')]
REGISTRATIONACT_CHOICE = [(1,'YES'),(2,'NO')]
INCOMETAXACT_12A_CHOICE = [(1,'YES'),(2,'NO')]
CSR_SECTION_135 = [(1,'YES'),(2,'NO')]
NON_CSR = [(1,'YES'),(2,'NO')]
GENDER_CHOICE = [('male','MALE'),('female','FEMALE'),('others','OTHERS')]
PROFESSION_CHOICE = [('business','BUSINESS'),('professional','PROFESSIONAL'),('service','SERVICE'),('others','OTHERS')]
PERSONAL_SKILLS_CHOICE = [(1,'BUSINESS'),(2,'SERVICE'),(3,'OTHERS')]
OTHERS_FORM_OF_ORGANISATION = [('1','Partnership Firm'),('2','Association of Person')]
STATUS = [('ACTIVE','ACTIVE'),('INACTIVE','INACTIVE'),('COMPLETED','COMPLETED')]


# Create your models here.
class CorporateCauses(models.Model):
	user_id = models.ForeignKey(UserProfile)
	cause = models.ForeignKey(Cause)
	sub_cause = models.ForeignKey(SubCause)
	cause_desc = models.CharField(max_length=255)

class IndividualCauses(models.Model):
	user_id = models.ForeignKey(UserProfile)
	cause = models.ForeignKey(Cause)
	sub_cause = models.ForeignKey(SubCause)
	cause_desc = models.CharField(max_length=255)

class OthersCauses(models.Model):
	user_id = models.ForeignKey(UserProfile)
	cause = models.ForeignKey(Cause)
	sub_cause = models.ForeignKey(SubCause)
	cause_desc = models.CharField(max_length=255)



class PersonalSkills(models.Model):
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" % (self.name)


class OthersFormOfOrganisation(models.Model):
	name = models.CharField(max_length=255)

class IntendToReceive(models.Model):
    user = models.ForeignKey(UserProfile)
    location = models.CharField(max_length=100,blank=True,null=True)
    community = models.ForeignKey(Community,blank=True,null=True)
    cause = models.ForeignKey(Cause, blank=True,null=True)
    cause_subcategory = models.ForeignKey(SubCause, blank=True,null=True)
    cause_description = models.CharField(max_length=500,blank=True,null=True)
    money = models.CharField(max_length=100,blank=True,null=True)
    money_frequency = models.ForeignKey(MoneyFrequency,blank=True,null=True)
    project_desc = models.TextField(blank=True,null=True)
    comment = models.TextField(blank=True,null=True)
    request_date = models.DateField(default=timezone.now,blank=True,null=True)
    status = models.CharField(choices=STATUS,max_length=255,blank=True,default=1)

    def __unicode__(self):
        return "%s" %(self.id)

    @property
    def get_mkt_donated(self):

		raised = dict()
		kind_list = []
		time_list = []
		kind_requested_list = []
		time_requested_list = []

		if self.money == "":
		    money_requested = float(0)
		else:
		    money_requested = float(self.money)


		kind_requested = float(0)
		time_requested = float(0)

		money_donated = float(0)
		kind_donated = float(0)
		time_donated = float(0)

		for kind in self.receivekind_set.all():
		    kind_requested = float(kind_requested) + float(kind.quantity)
		    kind_requested_list.append(kind)



		for time in self.receivetime_set.all():
		    time_requested = float(time_requested) + float(time.manhours)

		all_contributions = Contribution.objects.filter(request_id = self.id)
		 
		for contributions in all_contributions:
		    contribution = contributions
		    money_donated =  float(money_donated) + float(contribution.money)
		    for kind in contribution.kindcontribution_set.all():
		        kind_donated = float(kind_donated) + float(kind.quantity)
		        kind_list.append(kind)

		    for time in contribution.timecontribution_set.all():
		        time_donated = float(time_donated) + float(time.manhours)
		        time_list.append(time)

		        # for kind in kind_list:


		        if money_requested == 0.0:
		            completed_money = 0.0
		        else:
		            completed_money = float((money_donated/money_requested)*float(0.34))

		        if kind_requested == 0.0:
		            completed_kind = 0.0
		        else:
		            completed_kind = float((kind_donated/kind_requested)*float(0.33))

		        if time_requested == 0.0:
		            completed_time = 0.0
		        else:
		            completed_time = float((time_donated/time_requested)*float(0.33))

		        pending_money =  money_requested - money_donated
		        pending_kind =  kind_requested - kind_donated
		        pending_time =  time_requested - time_donated
		        raised['completed_money'] = completed_money
		        raised['completed_kind'] = completed_kind
		        raised['completed_time'] = completed_kind
		        raised['money_requested'] = money_requested
		        raised['kind_requested'] = kind_requested
		        raised['time_requested'] = time_requested

		        raised['pending_money'] = pending_money
		        raised['pending_kind'] = pending_kind
		        raised['pending_time'] = pending_time
		        raised['kind'] = kind_list
		        raised['time'] = time_list    

		        completed_donation = completed_money + completed_kind + completed_time
		        raised['completed_donation'] = completed_donation*100

		        return raised    


class ReceiveKind(models.Model):
    kind = models.ForeignKey(IntendToReceive,blank=True,null=True)
    quantity = models.IntegerField(blank=True,null=True)
    ktype = models.ForeignKey(KindType,blank=True,null=True)
    ksubtype = models.ForeignKey(KindSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(KindFrequency,blank=True,null=True)
    photo = models.FileField(upload_to="kind",blank=True,null=True)
    photo_tag = models.CharField(max_length=100,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.ktype)

class ReceiveTime(models.Model):
    time = models.ForeignKey(IntendToReceive,blank=True,null=True)
    manhours = models.IntegerField(blank=True,null=True)
    skilltype = models.ForeignKey(TimeSkillType,blank=True,null=True)
    skillsubtype = models.ForeignKey(TimeSkillSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(TimeFrequency,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.skilltype) 

class ReturnToCommunityObligations(models.Model):
	request_id = models.ForeignKey(IntendToReceive)
	time_manhours = models.IntegerField(blank=True,null=True)
	time_skill_type = models.ForeignKey(TimeSkillType)
	time_skill_subtype = models.ForeignKey(TimeSkillSubType)
	time_specifics = models.CharField(max_length=500,blank=True,null=True)
	time_frequency = models.ForeignKey(TimeFrequency)

	def __unicode__(self):
		return "%s" %(self.request_id)

class ReturnToCommunityCompleted(models.Model):
	request_id = models.ForeignKey(IntendToReceive)
	time_manhours = models.IntegerField(blank=True,null=True)

	def __unicode__(self):
		return "%s" %(self.request_id)

class CorporateFormOfOrganisation(models.Model):
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" %(self.name)

class NgoFormOfOrganisation(models.Model):
	name = models.CharField(max_length=255,blank=True,null=True)

	def __unicode__(self):
		return "%s" %(self.name)	

class NgoOfficeBearers(models.Model):
	user_id = models.ForeignKey(UserProfile)
	name = models.CharField(max_length=255)
	designation = models.CharField(max_length=255)
	email_id = models.CharField(max_length=255)
	mobile_no = models.CharField(max_length=255)
	photo = models.FileField(upload_to="cor_officebearers_pics",blank=True,null=True)
	description = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" %(self.name)

class OrganizationPurpose(models.Model):
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" %(self.name)

class OrganizationDetails(models.Model):
	user = models.ForeignKey(UserProfile)
	certificate_of_registration = models.FileField()
	purpose = models.ForeignKey(OrganizationPurpose,blank=True,null=True)
	donor_benefit_section = models.CharField(max_length=255,choices=DONOR_BENEFIT_SECTION_CHOICE,blank=True,null=True)
	deduction_rate = models.CharField(max_length=255,choices=RATE_OF_DEDUCTION_CHOICE,blank=True,null=True)
	religious_reason = models.BooleanField(blank=True)
	religious_reason_desc = models.CharField(max_length=255,blank=True,null=True)
	contactperson_name = models.CharField(max_length=255,blank=True,null=True)
	contactperson_emailid = models.CharField(max_length=255,blank=True,null=True)
	contactperson_mobileno = models.CharField(max_length=255,blank=True,null=True)
	contactperson_address = models.TextField(blank=True,null=True)
	office_bearers = models.ManyToManyField(NgoOfficeBearers,blank=True)

	def __unicode__(self):
		return "%s" %(self.user)


class NgoFinancialDetails(models.Model) :
	user = models.ForeignKey(UserProfile)
	financialdate = models.DateField()
	financial_year = models.CharField(max_length=50,blank=True,null=True)
	don_govt_nongovt = models.IntegerField(blank=True,null=True)
	don_india = models.IntegerField(blank=True,null=True)
	don_foreign = models.IntegerField(blank=True,null=True)
	don_khmembers = models.IntegerField(blank=True,null=True)
	don_assets = models.IntegerField(blank=True,null=True)
	otherincome = models.IntegerField(blank=True,null=True)
	grossincome = models.IntegerField(blank=True,null=True)
	expense_objective = models.IntegerField(blank=True,null=True)
	expense_administration = models.IntegerField(blank=True,null=True)
	expense_total = models.IntegerField(blank=True,null=True)
	net_surpluss_deficit = models.IntegerField(blank=True,null=True)
	
	def __unicode__(self):
		return "%s" %(self.user)

class AddPastProject(models.Model):
	user = models.ForeignKey(UserProfile)
	name = models.CharField(max_length=255,blank=False,null=True)
	location = models.ForeignKey('location.Location',blank=True,null=True)
	supported_by = models.CharField(max_length=255,blank=True)
	support_particulars = models.CharField(max_length=255,blank=True)
	financialyear = models.CharField(max_length=50,blank=True,null=True)
	money = models.IntegerField(blank=True,null=True)
	kind_desc = models.CharField(max_length=50,blank=True,null=True)
	time_desc = models.CharField(max_length=50,blank=True,null=True)
	direct_benefitter = models.CharField(max_length=100,blank=True,null=True)
	indirect_benefitter = models.CharField(max_length=100,blank=True,null=True)
	description = models.CharField(max_length=100,blank=True,null=True)

	def __unicode__(self):
		return "%s" %(self.user)

class NgoCauses(models.Model):
	user_id = models.ForeignKey(UserProfile)
	cause = models.ForeignKey(Cause)
	sub_cause = models.ForeignKey(SubCause)
	cause_desc = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" %(self.user_id)


class NgoBasic(models.Model):
	user = models.ForeignKey(UserProfile,blank=True,null=True)
	profile_geography_desc = models.TextField(blank=True,null=True)
	csr_ready = models.BooleanField(default=False,blank=True)
	ngo_image = models.ImageField(upload_to="entity/ngo",blank=True,null=True)
	incometaxact_12A_1023 = models.BooleanField(default=True,blank=True)

	def __unicode__(self):
		return "%s" %(self.user)


class CorporateBasic(models.Model):
	user_id = models.ForeignKey(UserProfile,blank=True,null=True)
	contactperson_name = models.CharField(max_length=255,blank=True,null=True)
	contactperson_emailid = models.CharField(max_length=255,blank=True,null=True)
	contactperson_mobileno = models.CharField(max_length=255,blank=True,null=True)
	contactperson_address = models.TextField(blank=True,null=True)
	company_image = models.ImageField(upload_to ='pictures/corporate',blank=True, null=True)
	company_pan =  models.CharField(max_length=10,blank=False,default="")
	csr_section_135 = models.BooleanField(default=False,blank=False)
	csr_fund = models.CharField(max_length=255,blank=True,null=True)
	non_csr = models.BooleanField(default=False)
	non_csr_fund = models.CharField(max_length=255,blank=True,null=True)
	non_csr_cause = models.CharField(max_length=255,blank=True,null=True)
	csr_objectives = models.TextField(blank=True,null=True)
	focusarea = models.TextField(blank=True,null=True)
	internal_process_desc = models.TextField(blank=True)
	selection_criteria = models.TextField(blank=True)
	nature_of_business = models.TextField(blank=True)
	profile_geography_desc = models.TextField(blank=True)
	company_history = models.TextField(blank=True)

	def __unicode__(self):
		return "%s" % (self.user_id)

class CorporateOfficeBearers(models.Model):
	officebearers_id = models.ForeignKey(CorporateBasic,blank=True,null=True)
	officebearers_name = models.CharField(max_length=255)
	officebearers_designation = models.CharField(max_length=255)
	officebearers_email_id = models.CharField(max_length=255)
	officebearers_mobile_no = models.CharField(max_length=255)
	officebearers_photo = models.FileField(upload_to="cor_officebearers_pics",blank=True,null=True)
	officebearers_description = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" % (self.officebearers_name)


class CorporateCSRCommitee(models.Model):
	csrcommitee_id = models.ForeignKey(CorporateBasic,blank=True,null=True)
	csrcommitee_name = models.CharField(max_length=255)
	csrcommitee_designation = models.CharField(max_length=255)
	csrcommitee_email_id = models.CharField(max_length=255)
	csrcommitee_mobile_no = models.CharField(max_length=255)
	csrcommitee_photo = models.FileField(upload_to="csr_committee_pics",blank=True,null=True)
	csrcommitee_description = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" % (self.csrcommitee_name)




class CsrApplicability(models.Model):
	applicability = models.ManyToManyField(CorporateBasic,max_length=255)


class IndividualDetails(models.Model):
	individual_user_id = models.ForeignKey(UserProfile,blank=True,null=True)
	individual_pan = models.CharField(max_length=20,blank=True)
	individual_aadhar_no = models.CharField(max_length=255,blank=True)
	individual_ckyc = models.CharField(max_length=255,blank=True)
	individual_occupation = models.CharField(max_length=255,choices=PROFESSION_CHOICE,default=1,blank=True)
	individual_business_dedsc = models.CharField(max_length=255,blank=True,null=True)
	individual_professional_skills = models.ManyToManyField(PersonalSkills,blank=True)
	individual_nature_of_profession = models.CharField(max_length=255,blank=True,null=True)
	individual_professional_email_id = models.EmailField(max_length=255,blank=True,null=True)
	individual_nature_of_job = models.CharField(max_length=200, blank=True,null=True)
	individual_name_of_employer = models.CharField(max_length=30, blank=True,null=True)
	individual_designation = models.CharField(max_length=30,blank=True,null=True)
	individual_others_details = models.CharField(max_length=255,blank=True,null=True)
	individual_personal_skills = models.ManyToManyField(PersonalSkills,related_name='personalskills',blank=True)
	individual_experience_skills = models.CharField(max_length=100,blank=True)
	individual_profile_geography_desc = models.TextField(blank=True)
	individual_cause = models.ManyToManyField(IndividualCauses,blank=True)


	def __unicode__(self):
		return "%s" %(self.individual_user_id)

class OthersDetails(models.Model):
	others_user_id = models.ForeignKey(UserProfile,blank=True,null=True)
	others_contactperson_name = models.CharField(max_length=255,blank=True,null=True)
	others_contactperson_emailid = models.CharField(max_length=255,blank=True,null=True)
	others_contactperson_mobileno = models.CharField(max_length=255,blank=True,null=True)
	others_contactperson_address = models.TextField(blank=True,null=True)
	others_aadhar_no = models.CharField(max_length=255,blank=False)
	others_pan = models.CharField(max_length=255,blank=True,null=True)
	others_ckyc = models.CharField(max_length=255,blank=True,null=True)
	others_alternate_contact = models.IntegerField(blank=True,null=True)
	others_business_nature = models.TextField(blank=True,null=True)
	others_profile_geography_desc = models.TextField(blank=True,null=True)
	others_cause = models.ManyToManyField(OthersCauses,blank=True)


	def __unicode__(self):
		return "%s" %(self.others_user_id)





