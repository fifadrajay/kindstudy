from django.contrib import admin

from .models import Bank, BankBranch, BankAccount, Remittance, DocumentCategory, Document

admin.site.register(Bank)
admin.site.register(BankBranch)
admin.site.register(BankAccount)
admin.site.register(Remittance)
admin.site.register(DocumentCategory)
admin.site.register(Document)