from __future__ import unicode_literals

from django.db import models
from datetime import datetime
from frontend.models import UserProfile

ACCOUNT_TYPE_CHOICES = (("Savings", "Savings"), ("Current", "Current"))

class Bank(models.Model):
    """
    This is Bank master references of which is used in 
    BorroweBank and LenderBank along with other models
    """
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class BankBranch(models.Model):
    """
    Bank with branch
    """
    bank = models.ForeignKey(Bank)
    branch = models.CharField(max_length=255)
    branch_address = models.CharField(max_length=255, blank=True, null=True)
    ifsc_code = models.CharField(max_length=100)

    def __unicode__(self):
        return self.bank.name+" | "+self.branch+" | "+self.ifsc_code


class BankAccount(models.Model):
    bank_branch = models.ForeignKey(BankBranch, null=True)
    account_holder = models.ForeignKey('frontend.UserProfile')
    account_number = models.CharField(max_length=100)
    account_type = models.CharField(
        max_length=25,
        choices=ACCOUNT_TYPE_CHOICES,
        default="Current")
    is_escrow_account = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s: %s, Account No:%s" % (
            self.account_holder.full_name(),
            self.bank_branch.bank,
            self.account_number)


class Remittance(models.Model):
    transaction_ts = models.DateTimeField(auto_now_add=True)
    remitted_by = models.ForeignKey('frontend.UserProfile')
    amount = models.DecimalField(max_digits=19, decimal_places=2)
    source_account = models.ForeignKey(BankAccount, related_name="source_account")
    destination_account = models.ForeignKey(BankAccount, related_name="destination_account")
    ref_transaction_id = models.CharField(max_length=255)


    def __unicode__(self):
        return "Remittance by %s on %s" % (self.remitted_by, self.transaction_ts)

class DocumentCategory(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

class Document(models.Model):
    category = models.ForeignKey(DocumentCategory)
    uploaded_by = models.ForeignKey(UserProfile)
    uploaded_on = models.DateTimeField(datetime.now())
    url = models.URLField()

    def __unicode__(self):
        return "%s uploaded by %s" % (self.category, self.uploaded_by)
