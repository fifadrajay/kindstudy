from django import forms
from django.forms import ModelForm,inlineformset_factory
from .models import *
from django.core.exceptions import ValidationError
from datetime import datetime
import os
import pdb



class FrequentQuestionsForm(ModelForm):

	class Meta:
		model = ProjectFaq
		exclude = []

class CreateProjectForm(ModelForm):
	
	class Meta:
		model = Project
		exclude = []

	def clean(self):
		cleaned_data = super(CreateProjectForm,self).clean()
		
		start_date = cleaned_data.get('start_date')
		end_date = cleaned_data.get('end_date')
		no_of_days = cleaned_data.get('no_of_days')
		short_desc = cleaned_data.get('short_desc')
		long_desc = cleaned_data.get('long_desc')
		title = cleaned_data.get('title')
		money = cleaned_data.get('money')
		money_frequency = cleaned_data.get('money_frequency')	

		kind_quantity = cleaned_data.get('kind-0-quantity')
		kind_type = cleaned_data.get('kind-0-ktype')
		kind_subtype = cleaned_data.get('kind_subtype')
		kind_specifics = cleaned_data.get('kind-0-specifics')
		kind_frequency = cleaned_data.get('kind-0-frequency')
		time_manhours = cleaned_data.get('time-0-manhours')
		time_skill_type = cleaned_data.get('time-0-skilltype')
		time_skill_subtype = cleaned_data.get('time-0-skillsubtype')
		time_specifics = cleaned_data.get('time-0-specifics')
		time_frequency = cleaned_data.get('time-0-frequency')

		project_image = cleaned_data.get('project_image')
		project_video = cleaned_data.get('project_video')

		

		if None in (start_date,end_date):
			msg = "Start Date Should be Smaller Than End Date"
			if start_date > end_date:
				self._errors["end_date"] = self.error_class([msg])
				raise forms.ValidationError('Start Date Should be Smaller Than End Date')

		if no_of_days is None and start_date is None and end_date is None:
			msg = "Choose proper Project Duration"
			self._errors['duration'] = self.error_class([msg])
				
		
		if short_desc is None:
			msg = "Short Description is Required"
			self._errors["short_desc"] = self.error_class([msg])

		if long_desc is None:
			msg = "Long Description is Required"
			self._errors["long_desc"] = self.error_class([msg])

		if title is None:
			msg = "Project title is Required"
			self._errors["title"] = self.error_class([msg])


		

		if project_image is not None:
			image_extention = str(project_image).split('.')	
			if image_extention[1] is not None and  image_extention[1] not in ['JPEG','JPG','PNG','jpeg','jpg','png']:
				image_msg = "Supported Image Format is JPEG, JPG, and PNG"
				self._errors["project_image"] = self.error_class([image_msg])

		if project_video is not None:
			video_extention = str(project_video).split('.')		
			if video_extention[1] not in ['AVI','FLV','WMV','MOV','MP4','avi','flv','wmv','mov','mp4']:
				video_msg = "Supported Image Format is AVI, FLV, WMV, MOV, MP4"
				self._errors["project_video"] = self.error_class([video_msg])

		

		if money == "" and money_frequency is not None:
			money_msg = "Cannot Select Money Frequency without selecting Money"
			self._errors["money"] = self.error_class([money_msg])

		# if kind_quantity is None and kind_type is not None or kind_subtype is not None  or kind_frequency is not None :
		# 	kind_msg =  "Cannot Select Kind Options without selecting Kind Quantity"
		# 	self._errors["kind-0-quantity"] = self.error_class([kind_msg])
		# if time_manhours is None and time_skill_type is not None or time_skill_subtype is not None  or time_frequency is not None:
		# 	time_msg =  "Cannot Select Time Options without selecting Time man/Hrs"
		# 	self._errors["time_manhours"] = self.error_class([time_msg])		

		return self.cleaned_data		


class ProjectKindForm(ModelForm):

	class Meta:
		model = ProjectKind
		exclude = ()

	def clean(self):
		cleaned_data = super(ProjectKindForm,self).clean()
		

		kind_quantity = cleaned_data.get('quantity')
		kind_type = cleaned_data.get('ktype')
		kind_subtype = cleaned_data.get('subtype')
		kind_specifics = cleaned_data.get('specifics')
		kind_frequency = cleaned_data.get('frequency')
		


		print kind_quantity 
		print kind_type
		print kind_subtype 
		print kind_specifics 
		print kind_frequency 
		

		if kind_quantity is None and (kind_type is not None or kind_subtype is not None or kind_specifics == None or kind_frequency is not None) :
			kind_msg =  "Cannot Select Kind Options without selecting Kind Quantity"
			self._errors["kind_quantity"] = self.error_class([kind_msg])

		


		return self.cleaned_data

ProjectKindFormSet = inlineformset_factory(Project,ProjectKind,form=ProjectKindForm)

class ProjectTimeForm(ModelForm):

	class Meta:
		model = ProjectTime
		exclude = ()

	def clean(self):
		cleaned_data = super(ProjectTimeForm,self).clean()

		time_manhours = cleaned_data.get('manhours')
		time_skill_type = cleaned_data.get('skilltype')
		time_skill_subtype = cleaned_data.get('skillsubtype')
		time_specifics = cleaned_data.get('specifics')
		time_frequency = cleaned_data.get('frequency')

		print time_manhours 
		print time_skill_type
		print time_skill_subtype
		print time_specifics
		print time_frequency

		if time_manhours is None and (time_skill_type is not None or time_skill_subtype is not None or time_specifics is not None or time_frequency is not None):
			time_msg =  "Cannot Select Time Options without selecting Time man/Hrs"
			self._errors["time_manhours"] = self.error_class([time_msg])


		return cleaned_data

ProjectTimeFormSet = inlineformset_factory(Project,ProjectTime,form=ProjectTimeForm)	


