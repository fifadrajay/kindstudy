from django.contrib import admin
from .models import *

admin.site.register(Project)
admin.site.register(ProjectFaq)
admin.site.register(ProjectUpdates)
admin.site.register(ProjectComment)
admin.site.register(ProjectKind)
admin.site.register(ProjectTime)
