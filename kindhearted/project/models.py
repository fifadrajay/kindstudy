from __future__ import unicode_literals

from django.db import models
from frontend.models import UserProfile
from contribution.models import MoneyFrequency,KindType,KindSubType,KindFrequency,TimeSkillType,TimeSkillSubType,TimeFrequency,Contribution
from cause.models import Cause,SubCause
from datetime import datetime
from django.utils import timezone
import pdb
class Project(models.Model):
    user_id = models.ForeignKey(UserProfile)
    title =  models.CharField(max_length=255,blank=False)
    short_desc = models.CharField(max_length=255,blank=False)
    long_desc = models.TextField()
    money = models.CharField(max_length=100,blank=True,null=True)
    money_frequency = models.ForeignKey(MoneyFrequency,blank=True,null=True)
    project_cause = models.ForeignKey(Cause)
    project_subcause = models.ForeignKey(SubCause)
    project_cause_specifics = models.CharField(max_length=255)
    location = models.CharField(max_length=255,blank=True, null=True)
    project_image = models.FileField(upload_to="project_pics",blank=True,null=True)
    project_video_link = models.CharField(max_length=255,blank=True,null=True)
    flg_duration = models.BooleanField(default=False,blank=False)
    no_of_days = models.IntegerField(null=True,blank=True,default=0)
    start_date = models.DateField(blank=True,null=True)
    end_date = models.DateField(blank=True,null=True)
    flg_approved = models.BooleanField(blank=True,default=False)
    approved_on = models.DateTimeField(blank=True,null=True,default=timezone.now)


    def __unicode__(self):
        return "%s" %(self.title)

    @property
    def get_mkt_donated(self):
        
        raised = dict()


        if self.money == "":
            money_requested = float(0)
        else:
            money_requested = float(self.money)

        kind_requested = float(0)
        time_requested = float(0)
        
        money_donated = float(0)
        kind_donated = float(0)
        time_donated = float(0)

        count_contribution = 0

        for kind in self.projectkind_set.all():
            kind_requested = float(kind_requested) + float(kind.quantity)
        for time in self.projecttime_set.all():
            time_requested = float(time_requested) + float(time.manhours)

        all_contributions = Contribution.objects.filter(project_id = self.id)
         
        for contributions in all_contributions:
            count_contribution = count_contribution + 1 
            contribution = contributions
            money_donated =  float(money_donated) + float(contribution.money)
            for kind in contribution.kindcontribution_set.all():
                kind_donated = float(kind_donated) + float(kind.quantity)

            for time in contribution.timecontribution_set.all():
                time_donated = float(time_donated) + float(time.manhours)

        if money_requested == 0.0:
            completed_money = 0.0
        else:
            completed_money = float((money_donated/money_requested)*float(0.34))

        if kind_requested == 0.0:
            completed_kind = 0.0
        else:
            completed_kind = float((kind_donated/kind_requested)*float(0.33))

        if time_requested == 0.0:
            completed_time = 0.0
        else:
            completed_time = float((time_donated/time_requested)*float(0.33))

        pending_money =  money_requested - money_donated
        pending_kind =  kind_requested - kind_donated
        pending_time =  time_requested - time_donated
        raised['completed_money'] = completed_money
        raised['completed_kind'] = completed_kind
        raised['completed_time'] = completed_kind
        raised['money_requested'] = money_requested
        raised['kind_requested'] = kind_requested
        raised['time_requested'] = time_requested

        raised['pending_money'] = pending_money
        raised['pending_kind'] = pending_kind
        raised['pending_time'] = pending_time

        raised['donated_money'] = money_donated
        raised['donated_kind'] = kind_donated
        raised['donated_time'] = time_donated
        raised['count_contribution'] = count_contribution
        completed_donation = completed_money + completed_kind + completed_time
        raised['completed_donation'] = completed_donation*100
        return raised
    

class ProjectKind(models.Model):
    kind = models.ForeignKey(Project,blank=True,null=True)
    quantity = models.IntegerField(blank=True,null=True)
    ktype = models.ForeignKey(KindType,blank=True,null=True)
    ksubtype = models.ForeignKey(KindSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(KindFrequency,blank=True,null=True)
    photo = models.FileField(upload_to="kind",blank=True,null=True)
    photo_tag = models.CharField(max_length=100,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.kind)

class ProjectTime(models.Model):
    time = models.ForeignKey(Project,blank=True,null=True)
    manhours = models.IntegerField(blank=True,null=True)
    skilltype = models.ForeignKey(TimeSkillType,blank=True,null=True)
    skillsubtype = models.ForeignKey(TimeSkillSubType,blank=True,null=True)
    specifics = models.CharField(max_length=255,blank=True,null=True)
    frequency = models.ForeignKey(TimeFrequency,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.time)


class ProjectFaq(models.Model):
    project_id = models.ForeignKey(Project)
    question = models.CharField(max_length=255)
    answer = models.CharField(max_length=255,blank=True,null=True)

    def __unicode__(self):
        return "%s" %(self.question)

class ProjectUpdates(models.Model):
    project_id = models.ForeignKey(Project)
    updated_by = models.ForeignKey(UserProfile)
    update_title = models.CharField(max_length=255)
    description =  models.CharField(max_length=255)
    update_on  = models.DateTimeField(datetime.now())

    def __unicode__(self):
        return "%s" %(self.updated_by)


class ProjectComment(models.Model):
    project_id =  models.ForeignKey(Project)
    commentor = models.CharField(max_length=255)
    comment =  models.TextField()

    def __unicode__(self):
        return "%s" %(self.commentor)
	#commented_on =  use activity stream package

    
# for remaining amount it is to be subtracted from goal amount which we will be changed 
# everyday so it will not be saved but write either view or js for it