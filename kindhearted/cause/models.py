from __future__ import unicode_literals

from django.db import models
from datetime import datetime

from frontend.models import UserProfile
from django.utils import timezone

class CauseCategory(models.Model):
    name = models.CharField(max_length=255, default="")
    it_clause = models.CharField(max_length=255,blank=True,null=True)
    gst_clause = models.CharField(max_length=255,blank=True,null=True)
    companies_act = models.CharField(max_length=255,blank=True,null=True)
    bombaypublictrust_act = models.CharField(max_length=255,blank=True,null=True)
    misc = models.CharField(max_length=255,blank=True,null=True)
    def __unicode__(self):
        return self.name

class Cause(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    category = models.ForeignKey(CauseCategory)
    active = models.BooleanField(default=False)
    cause_pic = models.FileField(upload_to="cause_pics",blank=True,null=True)
    cause_link = models.URLField()
    def __unicode__(self):
        return self.name

class SubCause(models.Model):
    name = models.CharField(max_length=255)
    cause = models.ForeignKey(Cause)
    category = models.ForeignKey(CauseCategory)
    subcause_pic = models.FileField(upload_to="cause_pics",blank=True,null=True)
    subcause_link = models.URLField()
    def __unicode__(self):
        return self.name

class Event(models.Model):
    name = models.CharField(max_length=255)
    cause = models.ForeignKey(Cause,blank=True,null=True)
    subcause = models.ForeignKey(SubCause,blank=True,null=True)
    created_by = models.ForeignKey(UserProfile)
    created_on = models.DateTimeField(default=timezone.now)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    photo = models.ImageField(upload_to="event_pics",blank=True,null=True)
    link = models.URLField()
    description = models.TextField(blank=True)

    def __unicode__(self):
        return "%s" %(self.name)



class PhotoGallery(models.Model):
    name = models.CharField(max_length=255)
    cause = models.ForeignKey(Cause,blank=True,null=True)
    subcause = models.ForeignKey(SubCause,blank=True,null=True)
    event = models.ForeignKey(Event,blank=True,null=True)
    created_by = models.ForeignKey(UserProfile)
    created_on = models.DateTimeField(blank=True,null=True,default=timezone.now)
    photo = models.ImageField(upload_to="gallery_pics", blank=True, null=True)

    def __unicode__(self):
        return self.name

class PhotoGalleryItem(models.Model):
    gallery = models.ForeignKey(PhotoGallery)
    uploaded_by = models.ForeignKey(UserProfile)
    uploaded_on = models.DateTimeField(datetime.now())
    pic = models.FileField(upload_to="gallery_pics", blank=True, null=True)
    def __unicode__(self):
        return self.gallery.name

class VideoGallery(models.Model):
    name = models.CharField(max_length=255)
    link = models.URLField()
    cause = models.ForeignKey(Cause,blank=True,null=True)
    subcause = models.ForeignKey(SubCause,blank=True,null=True)
    event = models.ForeignKey(Event,blank=True,null=True)
    created_by = models.ForeignKey(UserProfile)
    created_on = models.DateTimeField(blank=True,null=True,default=timezone.now)

    def __unicode__(self):
        return self.name

class VideoGalleryItem(models.Model):
    gallery = models.ForeignKey(VideoGallery)
    uploaded_by = models.ForeignKey(UserProfile)
    uploaded_on = models.DateTimeField(datetime.now())
    url = models.URLField()

    def __unicode__(self):
        return self.gallery.name


class Community(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s" %(self.name)