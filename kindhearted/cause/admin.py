from django.contrib import admin

from .models import *

admin.site.register(CauseCategory)
admin.site.register(Cause)
admin.site.register(SubCause)
admin.site.register(Event)
admin.site.register(PhotoGallery)
admin.site.register(PhotoGalleryItem)
admin.site.register(VideoGallery)
admin.site.register(VideoGalleryItem)
admin.site.register(Community)
