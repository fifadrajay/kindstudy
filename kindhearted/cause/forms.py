from django import forms
from django.forms import ModelForm
from .models import PhotoGallery, VideoGallery


class GalleryPhotoForm(ModelForm):
	
	class Meta:
		model = PhotoGallery
		exclude = []



class GalleryVideoForm(ModelForm):
	
	class Meta:
		model = VideoGallery
		exclude = []		


class MemberPhotoForm(ModelForm):
	
	class Meta:
		model = PhotoGallery
		exclude = []		


class MemberVideoForm(ModelForm):
	
	class Meta:
		model = VideoGallery
		exclude = []		